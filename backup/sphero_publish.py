#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped, Pose, Twist
from sensor_msgs.msg import Imu
import numpy as np
import math
import sys
import time


class movement:


    def callback(self,vel):

        #duration = float(rospy.get_rostime())
        current_time = time.time() - self.start_time
        print(current_time)
        move = Twist()
        speed = 40 #bits
        y_vel = speed*math.cos(2*math.pi*current_time)
        # x_vel^2 + y_vel^2 = speed**2 >> x_vel = square(speed - y_vel^2)
        x_vel = math.sqrt(speed**2 - y_vel**2)
        move.linear.x = x_vel
        move.linear.y = y_vel
        #move.position.z = 1.0
        self.pub_robot.publish(move)
             
            
           
    def __init__(self):
        self.start_time = time.time()
        #### Publisher and Subscribers ###
        self.pub_robot = rospy.Publisher("/cmd_vel", Twist, queue_size=10)
        rospy.Subscriber("/imu", Imu, self.callback,  queue_size = 10)

def start(args):
    '''Initializes and cleanup ros node'''
    rospy.init_node('callback', anonymous=True)
    call = movement()
    rospy.spin()
if __name__ == '__main__':
    start(sys.argv)

