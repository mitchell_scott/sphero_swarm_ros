import rospy
import message_filters 
from geometry_msgs.msg import PoseStamped, Twist, TwistStamped
from numpy import linalg as LA
import numpy as np
import time
import math
import sys


#global count
#count = 0
overall_position = []


class move_spheros:
    def __init__(self):
        self.robot_number = 8
        self.pub_dict = {}
        self.start_time = time.time()
        for i in range(1,self.robot_number+1):
            vel_topic_name = 'cmd_vel%s' %(i)
            callback_name = 'callback%s' % (i)
            sphero_name = 'sphero%s' % (i)

            self.pub_dict[sphero_name] = rospy.Publisher(vel_topic_name, Twist, queue_size = 10)

    def move(self):
        i = 1
        twist = Twist()
        key_list = list(self.pub_dict.keys())
        #r = rospy.Rate(10)
        while not rospy.is_shutdown(): 
            move_time = time.time() - self.start_time
            if i <= self.robot_number:
                number = i 
            elif i > self.robot_number:
                number = i % self.robot_number 
                if number == 0:
                    number = self.robot_number
            i = i + 1
            sphero_name = key_list[number-1]
            frequency = math.pi/3
            move = Twist()
            speed = 40 #bits

            x_vel = speed*math.cos(frequency*time.time())
            y_vel = speed*math.sin(frequency*time.time())
            heading = math.atan2(y_vel,x_vel)
            print('speed', 40)
            print('heading', heading)
            print(sphero_name)
            twist.linear.x = x_vel
            twist.linear.y = y_vel
            #r.sleep()
            #print(move_time)
            #self.pub_dict[sphero_name].publish(twist)



    def stop(self):
        i = 1
        twist = Twist()
        key_list = list(self.pub_dict.keys())
        for i in range(0,self.robot_number):
            sphero_name = 'sphero%s' % (i+1)
            frequency = math.pi/2
            move = Twist()
            speed = 0 #bits

            x_vel = 0
            y_vel = 0

            print(sphero_name)
            twist.linear.x = x_vel
            twist.linear.y = y_vel
            self.pub_dict[sphero_name].publish(twist)
        return True
if __name__ == '__main__':
    rospy.init_node('move', anonymous=True)
    m = move_spheros()

    try:
        m.move()
    except KeyboardInterrupt:
        stop = m.stop()
        if stop:
            sys.exit(1)
        else:
            stop = m.stop()
            sys.exit(1)
    


