#!/usr/bin/env python

#***********************************************************
#* Software License Agreement (BSD License)
#*
#*  Copyright (c) 2012, Melonee Wise
#*  All rights reserved.
#*
#*  Redistribution and use in source and binary forms, with or without
#*  modification, are permitted provided that the following conditions
#*  are met:
#*
#*   * Redistributions of source code must retain the above copyright
#*     notice, this list of conditions and the following disclaimer.
#*   * Redistributions in binary form must reproduce the above
#*     copyright notice, this list of conditions and the following
#*     disclaimer in the documentation and/or other materials provided
#*     with the distribution.
#*
#*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#*  POSSIBILITY OF SUCH DAMAGE.
#***********************************************************
#author: Melonee Wise
#edited: Mitchell Scott December 4 2016 for multiple sphero useage
import sys
import rospy
import math
import sys
import tf
import PyKDL
from bluetooth import *



from sphero_driver import sphero_driver2 as sphero_driver
import dynamic_reconfigure.server

from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig

class sphero_node():

    battery_state =  {1:"Battery Charging",
                      2:"Battery OK",
                      3:"Battery Low",
                      4:"Battery Critical"}


    ODOM_POSE_COVARIANCE = [1e-3, 0, 0, 0, 0, 0, 
                            0, 1e-3, 0, 0, 0, 0,
                            0, 0, 1e6, 0, 0, 0,
                            0, 0, 0, 1e6, 0, 0,
                            0, 0, 0, 0, 1e6, 0,
                            0, 0, 0, 0, 0, 1e3]


    ODOM_TWIST_COVARIANCE = [1e-3, 0, 0, 0, 0, 0, 
                             0, 1e-3, 0, 0, 0, 0,
                             0, 0, 1e6, 0, 0, 0,
                             0, 0, 0, 1e6, 0, 0,
                             0, 0, 0, 0, 1e6, 0,
                             0, 0, 0, 0, 0, 1e3]
    def __init__(self, default_update_rate=50.0):
        rospy.init_node('sphero')
        self.robot_number = 3
        self.sphero_dict = {}
        self.sphero_name_list = ['Sphero-GGR','Sphero-BOP','Sphero-WPW']
        self.sphero_address_list = ['68:86:E7:08:99:C3','68:86:E7:08:AD:B5','68:86:E7:08:60:0A']
        self.sphero_port_list = [1,1,1]
        self.update_rate = default_update_rate
        self.sampling_divisor = int(400/self.update_rate)

        self.is_connected = False
        self._init_pubsub()
        self._init_params()
        #self.robot = sphero_driver.Sphero()
        self.imu = Imu()
        self.imu.orientation_covariance = [1e-6, 0, 0, 0, 1e-6, 0, 0, 0, 1e-6]
        self.imu.angular_velocity_covariance = [1e-6, 0, 0, 0, 1e-6, 0, 0, 0, 1e-6]
        self.imu.linear_acceleration_covariance = [1e-6, 0, 0, 0, 1e-6, 0, 0, 0, 1e-6]
        self.last_cmd_vel_time = rospy.Time.now()
        self.last_diagnostics_time = rospy.Time.now()
        self.cmd_heading = 0
        self.cmd_speed = 0
        self.power_state_msg = "No Battery Info"
        self.power_state = 0
        '''
        Create dictionary of spheros
        '''
        for i in range(0,self.robot_number):
            sphero_name = 'sphero%s' % (i+1)
            print(sphero_name)
            self.sphero_dict[sphero_name] = sphero_driver.Sphero(self.sphero_name_list[i],self.sphero_address_list[i],self.sphero_port_list[i])
            #self.sphero1 = sphero_driver.Sphero('Sphero-GGR','68:86:E7:08:99:C3',1)
            #self.sphero2 = sphero_driver.Sphero('Sphero-BOP','68:86:E7:08:AD:B5',2)

    def _init_pubsub(self):
        pass
        #self.odom_pub = rospy.Publisher('odom', Odometry, queue_size = 10)
        #self.imu_pub = rospy.Publisher('imu', Imu, queue_size = 10)
        #self.collision_pub = rospy.Publisher('collision', SpheroCollision, queue_size = 10)
        #self.diag_pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size = 10)
        #self.cmd_vel_sub = rospy.Subscriber('cmd_vel', Twist, self.cmd_vel, queue_size = 1)
        #self.color_sub = rospy.Subscriber('set_color', ColorRGBA, self.set_color, queue_size = 1)
        #self.back_led_sub = rospy.Subscriber('set_back_led', Float32, self.set_back_led, queue_size = 1)
        #self.stabilization_sub = rospy.Subscriber('disable_stabilization', Bool, self.set_stabilization, queue_size = 1)
        #self.heading_sub = rospy.Subscriber('set_heading', Float32, self.set_heading, queue_size = 1)
        #self.angular_velocity_sub = rospy.Subscriber('set_angular_velocity', Float32, self.set_angular_velocity, queue_size = 1)
        #self.reconfigure_srv = dynamic_reconfigure.server.Server(ReconfigConfig, self.reconfigure)
        #self.transform_broadcaster = tf.TransformBroadcaster()
    def _init_params(self):
        self.connect_color_red = rospy.get_param('~connect_red',0)
        self.connect_color_blue = rospy.get_param('~connect_blue',0)
        self.connect_color_green = rospy.get_param('~connect_green',255)
        self.cmd_vel_timeout = rospy.Duration(rospy.get_param('~cmd_vel_timeout', 0.6))
        self.diag_update_rate = rospy.Duration(rospy.get_param('~diag_update_rate', 1.0))

    def set_color(self, msg):
        #print('ONE')
        if self.is_connected:
            #print('True')
            self.sphero_dict['sphero1'].set_rgb_led(int(msg.r*255),int(msg.g*255),int(msg.b*255),0,False)

    def start(self):
        for i in range(0,self.robot_number):
            sphero_name = 'sphero%s' % (i+1)
            self.sphero_dict[sphero_name].connect()
        #print(self.sphero)
        #self.sphero1.connect()
        #self.sphero2.connect()
        self.is_connected = True
        self.color_sub = rospy.Subscriber('set_color', ColorRGBA, self.set_color, queue_size = 1)


        
if __name__ == '__main__':
    s = sphero_node()
    s.start()
    #if s.start():
    #    print('Connected')
    rospy.spin()
