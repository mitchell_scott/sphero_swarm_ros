#!/usr/bin/env python

#***********************************************************
#* Software License Agreement (BSD License)
#*
#*  Copyright (c) 2012, Melonee Wise
#*  All rights reserved.
#*
#*  Redistribution and use in source and binary forms, with or without
#*  modification, are permitted provided that the following conditions
#*  are met:
#*
#*   * Redistributions of source code must retain the above copyright
#*     notice, this list of conditions and the following disclaimer.
#*   * Redistributions in binary form must reproduce the above
#*     copyright notice, this list of conditions and the following
#*     disclaimer in the documentation and/or other materials provided
#*     with the distribution.
#*
#*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#*  POSSIBILITY OF SUCH DAMAGE.
#***********************************************************
#author: Mitchell Scott, with useage of sphero_ros work by Melonee Wise

import sys
import rospy
import math
import tf
import PyKDL
import bluetooth



from sphero_driver import sphero_driver2 as sphero_driver
import dynamic_reconfigure.server
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig
from sphero_class import sphero_start

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'


class start:
    def __init__(self):
        #Inital Information
        self.is_connected = False
        #Find nearby sphero 
        self.target_name = 'Sphero' #this allows for bluetooth to lookup local spheros
        self.name_list = []
        self.address_list = []
        self.color_topic_list = []
        self.vel_topic_list = []
        self.odom_topic_list = []
        self.imu_topic_list = []
        self.robot_list = []
        self.port_list = []
        self.sphero_dict = {} #to start the spheros/send data via. bluetooth
        self.robot_dict = {}  #to subscribe/publish topics
        self.nearby_devices = bluetooth.discover_devices(lookup_names = True)

        if len(self.nearby_devices)>0:

            self.address_list = ['68:86:E7:08:21:6C', '68:86:E7:08:A9:D8','68:86:E7:08:60:7D','68:86:E7:08:02:C1','68:86:E7:08:7F:41','68:86:E7:08:A9:B1','68:86:E7:08:AA:EE']
            #self.address_list = ['68:86:E7:08:A9:D8', '68:86:E7:08:02:C1']
            self.name_list = ['Sphero-RRY','Sphero-BWG','Sphero-ROR','Sphero-BPR','Sphero-BRY','Sphero-WWB','Sphero-OPY']
	    #self.name_list = ['Sphero-BWG','Sphero-BPR']
            self.port_list = [1,1,1,1,1,1,1]
            #self.port_list = [1,1]
            print('Devices to connect to:      ')
            print('name_list', self.name_list)
            print('address', self.address_list)
            print('I found %s nearby devices') % (len(self.name_list)) 
            for i in range(0,len(self.name_list)):
                color_topic = 'sphero/color%s' % (i+1)
                vel_topic = 'sphero/velocity%s' % (i+1)
                odom_topic = 'sphero/odom%s' % (i+1)
                imu_topic = 'sphero/imu%s' % (i+1)
                self.color_topic_list.append(color_topic)
                self.vel_topic_list.append(vel_topic)
                self.odom_topic_list.append(odom_topic)
                self.imu_topic_list.append(imu_topic)

        self.connected_list = [False]*len(self.address_list) #List which tell the actual robots which connected
    def begin(self):
        for i in range(0,len(self.name_list)):
            sphero_name = 'sphero%s' % (i+1)
            self.sphero_dict[sphero_name] = sphero_driver.Sphero(self.name_list[i], self.address_list[i], self.port_list[i])

    def initalize(self):
        for i in range(0,len(self.name_list)):
            sphero_name = 'sphero%s' % (i+1)
            self.robot_list.append(sphero_name)
            self.robot_dict[sphero_name] = sphero_start() #With the number of spheros defined by the sphero class, we can now initalzie topics for each of the spheros
            initalize = self.robot_dict[sphero_name].initalize(self.sphero_dict[sphero_name]) #Must run for the spheros to actually connect via. bluetooth
           
            if initalize:
               
                self.connected_list[i] = True #if it connected, set to true
                print('%s is connected') % (sphero_name)
                #self.robot_dict[sphero_name].call_color(self.color_topic_list[i]) #subscriber. User can publish to /sphero/color# in RGBA message type to change colors
                '''
                NOTE: call_velocity publication is in bits/255 with a max speed of 2.5 m/s. So a command of 50 is equal to 50/255 bits * 2.5 == .49 m/s
                '''
                #self.robot_dict[sphero_name].call_velocity(self.vel_topic_list[i]) #subscriber. User can publish to /sphero/vel# in Twist message type to change speed
       
                ##DONT EDIT BELOW THIS LINE. NOT WORKING##
                #TODO: IMU/odom
                '''
                Start up odometry and Imu data
                '''
                #robot_dict[sphero_name].call_odom_imu(odom_topic_list[i], imu_topic_list[i]) #subscriber. User can publish to /sphero/vel# in Twist message type to change speed

        topic = self.topic_start(self.connected_list)
        if topic:
          return True
    def topic_start(self,connected_list):
        self.connected_list = connected_list
        sphero_connection_list = [i for i,j in enumerate(self.robot_list) if self.connected_list[i] == True]
        print(sphero_connection_list)
        new_robot_list = [j for i,j in enumerate(self.robot_list) if self.connected_list[i] == True]
        print(new_robot_list)
        for i in range(0,len(new_robot_list)): 
            sphero_name = new_robot_list[i] 
            color_topic = 'sphero/color%s' % (sphero_connection_list[i] + 1)
            vel_topic = 'sphero/vel%s' % (sphero_connection_list[i] + 1)
            odom_topic = 'sphero/odom%s' % (sphero_connection_list[i] + 1)
            imu_topic = 'sphero/imu%s' % (sphero_connection_list[i] + 1)
            print('color topic', color_topic)
            call_color = self.robot_dict[sphero_name].call_color(color_topic)
            call_velocity = self.robot_dict[sphero_name].call_velocity(vel_topic)
        return True


    def connect_again(self):
        print('in connect again')
        try_again_list = [i for i,j in enumerate(self.robot_list) if self.connected_list[i] == False]
        for i in range(0,len(try_again_list)):
            #print(try_again_list)
            location = try_again_list[i]
            sphero_name = 'sphero%s' % (location + 1)
            #print('connecting to:', sphero_name)
            #print('name',self.name_list[location] )
            #print('address', self.address_list[location])
            self.sphero_dict[sphero_name] = sphero_driver.Sphero(self.name_list[location], self.address_list[location], self.port_list[location])
            initalize2 = self.initalize_again(location, sphero_name, self.sphero_dict)
            if initalize2:
                print('Initalize again location', location)
                self.connected_list[location] = True
                #print(i, self.connected_list)
        #for i in range(0,len(self.connected_list)):
        #    if self.connected_list[i] != True:
        #        print('False')
        #        break
        #    else:
        #        print('True')
        #        return True


    def initalize_again(self, location, sphero_name, sphero_dictionary):
        self.sphero_dictionary = sphero_dictionary
        self.robot_dict[sphero_name] = sphero_start() #With the number of spheros defined by the sphero class, we can now initalzie topics for each of the spheros
        initalize = self.robot_dict[sphero_name].initalize(self.sphero_dict[sphero_name]) #Must run for the spheros to actually connect via. bluetooth
        if initalize:
            topic = self.topic_start_again(location, sphero_name)
            if topic:
                return True

    def topic_start_again(self, location, sphero_name): 
        color_topic = 'sphero/color%s' % (location)
        vel_topic = 'sphero/vel%s' % (location)
        odom_topic = 'sphero/odom%s' % (location)
        imu_topic = 'sphero/imu%s' % (location)
        print('color topic', color_topic)
        call_color = self.robot_dict[sphero_name].call_color(color_topic)
        call_velocity = self.robot_dict[sphero_name].call_velocity(vel_topic)
        return True

               

if __name__ == '__main__':
    connect = False
    start = start()
    on = start.begin()
    #topic_start(color_topic_list,vel_topic_list,odom_topic_list,imu_topic_list)
    inital_start = start.initalize()
    j = 0
    for i in range(0,10):
        print(start.connected_list, i)
        print('j', j)
        while j < 6:
            #TODO: Why can this not recognize len(connected list)?
            print(start.connected_list[j], j)
            if start.connected_list[j] == False:
                print('false')
                connect = start.connect_again()
            j+=1
    if inital_start:
        print(color.GREEN + 'Everything is great! Go Cougs! You may move the spheros now.' + color.END)
        rospy.spin()


