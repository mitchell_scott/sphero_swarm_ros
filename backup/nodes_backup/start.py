#!/usr/bin/env python

#***********************************************************
#* Software License Agreement (BSD License)
#*
#*  Copyright (c) 2012, Melonee Wise
#*  All rights reserved.
#*
#*  Redistribution and use in source and binary forms, with or without
#*  modification, are permitted provided that the following conditions
#*  are met:
#*
#*   * Redistributions of source code must retain the above copyright
#*     notice, this list of conditions and the following disclaimer.
#*   * Redistributions in binary form must reproduce the above
#*     copyright notice, this list of conditions and the following
#*     disclaimer in the documentation and/or other materials provided
#*     with the distribution.
#*
#*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#*  POSSIBILITY OF SUCH DAMAGE.
#***********************************************************
#author: Mitchell Scott, with useage of sphero_ros work by Melonee Wise
import sys

import sys
import rospy
import math
import sys
import tf
import PyKDL
import bluetooth
import time
from bluetooth_streaming import many_streaming


from sphero_driver import sphero_driver2 as sphero_driver
import dynamic_reconfigure.server

from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig

from sphero_class import sphero_start
RECV = dict(
  ASYNC = [chr(0xff), chr(0xfe)],
  SYNC = [chr(0xff), chr(0xff)])
print('async',RECV['ASYNC'])
print('sync',RECV['SYNC'])
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'


class start:
    def __init__(self):
        #Inital Information
        self.is_connected = False
        #Find nearby sphero 
        self.target_name = 'Sphero' #this allows for bluetooth to lookup local spheros
        self.name_list = []
        self.address_list = []
        self.color_topic_list = []
        self.vel_topic_list = []
        self.odom_topic_list = []
        self.imu_topic_list = []
        self.robot_list = []
        self.port_list = []
        self.sphero_dict = {} #to start the spheros/send data via. bluetooth
        self.robot_dict = {} #to subscribe/publish topics
        self.nearby_devices = bluetooth.discover_devices(lookup_names = True)
        print(self.nearby_devices)
        self.call_color = False
        self.call_velocity = False
        self.call_imu = call_imu_odom = False
        self.connected_robots_list = []
        self.failed_robots_list = []
        if len(self.nearby_devices)>0:
            for bdaddr, name in self.nearby_devices:
                #look for a device name that starts with Sphero
                if name.startswith(self.target_name):
                    #if name != 'Sphero-RRY' and name != 'Sphero-BWG' and name != 'Sphero-BPR' and name != 'Sphero-ROY' and name != 'Sphero-BRY' and name != 'Sphero-WWB' and name != 'Sphero-OPY':
                    if name == 'Sphero-WBR' or name == 'Sphero-ORY' or name == 'Sphero-PRY' or name == 'Sphero-RRB' or name == 'Sphero-BBR' or name == 'Sphero-WRP' or name == 'Sphero-GRR' or name == 'Sphero-GRW' or name == 'Sphero-BOP' or name == 'Sphero-WWB' or name == 'Sphero-WBR':
                        self.name_list.append(name)
                        self.address_list.append(bdaddr)
                        if len(self.name_list) <= 7:
                            self.port_list.append(1)  #Tell which bluetooth port to connect to
                        elif len(self.name_list) > 7:
                           self.port_list.append(2)
            print('Devices to connect to:      ')
            print('name_list', self.name_list)
            print('address', self.address_list)
            #print('I found %s nearby devices') % (len(self.nearby_devices)) 
            #self.address_list = ['68:86:E7:09:0D:36', '68:86:E7:08:F5:82']
            #self.name_list = ['Sphero-GRW', 'Sphero-GRR']
            #self.port_list = [1,1]
            for i in range(0,len(self.name_list)):
                color_topic = 'sphero/color%s' % (i+1)
                vel_topic = 'sphero/velocity%s' % (i+1)
                odom_topic = 'sphero/odom%s' % (i+1)
                imu_topic = 'sphero/imu%s' % (i+1)
                self.color_topic_list.append(color_topic)
                self.vel_topic_list.append(vel_topic)
                self.odom_topic_list.append(odom_topic)
                self.imu_topic_list.append(imu_topic)
        self.connected_list = [False]*len(self.nearby_devices) #List which tell the actual robots which connected

    '''
    Tell the sphero drive the name address and port to connect to
    '''
    def begin(self):
        for i in range(0,len(self.name_list)):
            sphero_name = 'sphero%s' % (i+1)
            self.sphero_dict[sphero_name] = sphero_driver.Sphero(i+1, self.name_list[i], self.address_list[i], self.port_list[i])
        return True
    '''
    Connect the spheros and connect to topics
    '''
    def initalize(self):
        for i in range(0,len(self.name_list)):
            color_topic = 'sphero/color%s' % (i+1)
            vel_topic = 'sphero/vel%s' % (i+1)
            sphero_name = 'sphero%s' % (i+1)
            odom_topic = 'sphero/odom%s' % (i+1)
            imu_topic = 'sphero/imu%s' % (i+1)
            self.robot_list.append(sphero_name)
            #self.robot_dict[sphero_name] = sphero_start(i+1)
            self.robot_dict[sphero_name] = sphero_start(i+1, self.name_list[i], self.address_list[i], self.port_list[i])
            #initalize = self.robot_dict[sphero_name].initalize(self.sphero_dict[sphero_name]) #Must run for the spheros to actually connect via. bluetooth
            initalize = self.robot_dict[sphero_name].initalize()
            
            if initalize:
               self.connected_list[i] = True #if it connected, set to true
             #   print('%s is connected') % (sphero_name)
             #   self.connected_robots_list.append(sphero_name)
                #robot.spin()
            #else:
                #self.failed_robots_list.append(sphero_name)
        #self.new_robot_list = [j for i,j in enumerate(self.robot_list) if self.connected_list[i] == True]
        #self.spin(self.connected_list)

        #topic = self.topic_start(self.connected_list)
        #if topic:
            #return True
    def topic_start(self,connected_list):
        self.connected_list = connected_list
        color_topic_list_reduced = [j for i,j in enumerate(self.color_topic_list) if self.connected_list[i] == True]
        vel_topic_list_reduced = [j for i,j in enumerate(self.vel_topic_list) if self.connected_list[i] == True]
        self.new_robot_list = [j for i,j in enumerate(self.robot_list) if self.connected_list[i] == True]
        for i in range(0,len(color_topic_list_reduced)): 
            sphero_name = self.new_robot_list[i] 
            color_topic = 'sphero/color%s' % (i+1)
            vel_topic = 'sphero/vel%s' % (i+1)
            collision_topic = 'sphero/collision%s' % (i+1)
            self.call_color = self.robot_dict[sphero_name].call_color(color_topic)
            self.call_velocity = self.robot_dict[sphero_name].call_velocity(vel_topic)
            self.call_collision = self.robot_dict[sphero_name].call_collision(collision_topic)
            #self.robot_dict[sphero_name].spin()
            #self.robot_dict[sphero_name].stop()
        return True

    def odom_imu(self,connected_list):
        print('robot', self.robot_list)

        self.new_robot_list = [j for i,j in enumerate(self.robot_list) if self.connected_list[i] == True]
        print('new list', self.new_robot_list)
        for i in range(0,len(self.new_robot_list)): 
            odom_topic = 'sphero/odom%s' % (i+1)
            imu_topic = 'sphero/imu%s' % (i+1)
            diag_topic = 'sphero/diag%s' % (i+1)
            sphero_name = self.new_robot_list[i] 
            #self.call_imu =self.robot_dict[sphero_name].call_odom_imu(odom_topic, imu_topic, diag_topic)
            self.call_imu =self.robot_dict[sphero_name].call_odom_imu(i+1,len(self.new_robot_list))

    def spin(self,connected_list):
        self.connected_list = connected_list
        print('spin')
        for i in range(0,len(self.connected_list)): 
            print(i)
            sphero_name = self.new_robot_list[i] 
            self.robot_dict[sphero_name].spin()

    def stop(self,connected_list):
        self.connected_list = connected_list
        for i in range(0,len(self.connected_list)): 
            sphero_name = self.new_robot_list[i] 
            self.robot_dict[sphero_name].stop() 

    def run(self,connected_list):
        #r = rospy.Rate(10)
        for i in range(0,len(self.new_robot_list)):
            print(i)
            #while not rospy.is_shutdown():
            sphero_name = self.new_robot_list[i] 
            self.robot_dict[sphero_name].run()  
        #r.sleep()   

    def data_run(self,connected_list):
        organize = many_streaming()
        organize.organize(self.robot_dict)          

if __name__ == '__main__':
    print("Searching")
    rospy.init_node('sphero')
    start = start()
    #on = start.begin()
    #sock=bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    #sock.close() #close to remove socket data
    #topic_start(color_topic_list,vel_topic_list,odom_topic_list,imu_topic_list)
    #while not rospy.is_shutdown():
    print("Connecting ... be patient")
    inital_start = start.initalize()
    start.odom_imu(start.connected_robots_list)
    start.data_run(start.connected_robots_list)
    #start.run(start.connected_robots_list)
    #rospy.spin()











    """
    try:
        if inital_start and len(start.connected_robots_list) >= 1:
            start.odom_imu(start.connected_robots_list)
            #start.stop(start.connected_robots_list)
            #start.spin(start.connected_robots_list)

        if inital_start and len(start.connected_robots_list) < 1:
            print(color.RED + 'Failure, could not connect to any spheros. Exciting. Check bluetooth connection, try again.' + color.END)
            sys.exit(1)

        print('Connected to the following spheros:', start.connected_robots_list)
        time.sleep(2)
        if len(start.failed_robots_list) >=1:
            print('Failed to connect to the following spheros:', start.failed_robots_list)
        print(color.GREEN + 'Everything is great! Go Cougs! You may move the spheros now.' + color.END)
        #rospy.spin()
    except KeyboardInterrupt:
        print(color.RED + 'Failure. Exciting. Check bluettoth connection, try again.' + color.END)
        sys.exit(1) 
    
    try:

        start.spin(start.connected_robots_list)
        start.stop(start.connected_robots_list)
    except KeyboardInterrupt:
        print(color.RED + 'Failure. Exciting. Check bluettoth connection, try again.' + color.END)
        sys.exit(1)
    """
        














