#!/usr/bin/python
from sphero_driver import sphero_driver
import time
import rospy
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
import bluetooth
import sys


rospy.init_node('sphero')


address1 = '68:86:E7:08:99:C3'
address2 = '68:86:E7:08:AD:B5'
sock1=bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock2=bluetooth.BluetoothSocket(bluetooth.RFCOMM)

try:
    sock1.connect((address1,1))  
    #sock2.connect((address2,2))
except:
    print("error")
    sys.exit(1)

print('Worked')
rospy.spin()
