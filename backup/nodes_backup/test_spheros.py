import rospy
import message_filters 
from geometry_msgs.msg import PoseStamped, Twist, TwistStamped
from numpy import linalg as LA
import numpy as np
import time
import math
import sys


#global count
#count = 0
overall_position = []


class move_spheros:
    def __init__(self):
        self.robot_number = 1
        self.pub_dict = {}
        self.start_time = time.time()
        for i in range(1,self.robot_number+1):
            vel_topic_name = 'sphero/vel%s' %(i)
            callback_name = 'callback%s' % (i)
            sphero_name = 'sphero%s' % (i)
            #print(vel_topic_name)
            self.pub_dict[sphero_name] = rospy.Publisher(vel_topic_name, Twist, queue_size = 10)

    def move(self):
        twist = Twist()
        #r=rospy.Rate(10)
        current_time = time.time()
        move_time = time.time() - self.start_time
        for i in range(1,self.robot_number+1):
            sphero_name = 'sphero%s' % (i)
            frequency = math.pi/2
            move = Twist()
            speed = 50 #bits
            '''
            x_vel^2 + y_vel^2 = speed**2 >> x_vel = square(speed - y_vel^2)
            '''
            #print('name',sphero_name)
            x_vel = speed*math.cos(frequency*move_time)
            y_vel = speed*math.sin(frequency*move_time)

            print('x', x_vel)
            print('y', y_vel)
            #print('norm', math.sqrt(x_vel**2 + y_vel**2))
            twist.linear.x = x_vel
            twist.linear.y = y_vel
            #move.position.z = 1.0
            self.pub_dict[sphero_name].publish(twist)
            #print('here')
    def move(self):
        twist = Twist()
        #r=rospy.Rate(10)
        current_time = time.time()
        move_time = time.time() - self.start_time
        for i in range(1,self.robot_number+1):
            sphero_name = 'sphero%s' % (i)
            frequency = math.pi/2
            move = Twist()
            speed = 50 #bits
            '''
            x_vel^2 + y_vel^2 = speed**2 >> x_vel = square(speed - y_vel^2)
            '''
            #print('name',sphero_name)
            x_vel = speed*math.cos(frequency*move_time)
            y_vel = speed*math.sin(frequency*move_time)

            print('x', x_vel)
            print('y', y_vel)
            #print('norm', math.sqrt(x_vel**2 + y_vel**2))
            twist.linear.x = x_vel
            twist.linear.y = y_vel
            #move.position.z = 1.0
            self.pub_dict[sphero_name].publish(twist)
            #print('here')
    def move(self):
        twist = Twist()
        #r=rospy.Rate(10)
        current_time = time.time()
        move_time = time.time() - self.start_time
        for i in range(1,self.robot_number+1):
            sphero_name = 'sphero%s' % (i)
            frequency = math.pi/2
            move = Twist()
            speed = 50 #bits
            '''
            x_vel^2 + y_vel^2 = speed**2 >> x_vel = square(speed - y_vel^2)
            '''
            #print('name',sphero_name)
            x_vel = speed*math.cos(frequency*move_time)
            y_vel = speed*math.sin(frequency*move_time)

            print('x', x_vel)
            print('y', y_vel)
            #print('norm', math.sqrt(x_vel**2 + y_vel**2))
            twist.linear.x = x_vel
            twist.linear.y = y_vel
            #move.position.z = 1.0
            self.pub_dict[sphero_name].publish(twist)
            #print('here')


    def stop(self):
        twist = Twist()
        #r=rospy.Rate(10)
        current_time = time.time()
        for i in range(1,self.robot_number+1):
            sphero_name = 'sphero%s' % (i)
            current_time = time.time() - self.start_time
            frequency = math.pi/2
            move = Twist()
            speed = 0 #bits
            '''
            x_vel^2 + y_vel^2 = speed**2 >> x_vel = square(speed - y_vel^2)
            '''
            print('name',sphero_name)
            y_vel = speed*math.sin(frequency*current_time)
            x_vel = speed*math.cos(frequency*current_time)
            print('x', x_vel)
            print('y', y_vel)
            print('norm', math.sqrt(x_vel**2 + y_vel**2))
            twist.linear.x = x_vel
            twist.linear.y = y_vel
            #move.position.z = 1.0
            self.pub_dict[sphero_name].publish(twist)
            #print('here')
 
 
if __name__ == '__main__':
    rospy.init_node('move', anonymous=True)
    m = move_spheros()
    while not rospy.is_shutdown(): 
        try:
            m.move()
        except KeyboardInterrupt:
            m.stop()
            sys.exit(1)
    
    #rospy.spin()


