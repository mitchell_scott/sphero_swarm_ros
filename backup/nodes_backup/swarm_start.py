#!/usr/bin/env python

#***********************************************************
#* Software License Agreement (BSD License)
#*
#*  Copyright (c) 2012, Melonee Wise
#*  All rights reserved.
#*
#*  Redistribution and use in source and binary forms, with or without
#*  modification, are permitted provided that the following conditions
#*  are met:
#*
#*   * Redistributions of source code must retain the above copyright
#*     notice, this list of conditions and the following disclaimer.
#*   * Redistributions in binary form must reproduce the above
#*     copyright notice, this list of conditions and the following
#*     disclaimer in the documentation and/or other materials provided
#*     with the distribution.
#*
#*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#*  POSSIBILITY OF SUCH DAMAGE.
#***********************************************************
#author: Mitchell Scott, with useage of sphero_ros work by Melonee Wise
import sys

import sys
import rospy
import math
import sys
import tf
import PyKDL
import bluetooth
import time




from sphero_driver import sphero_driver2 as sphero_driver
import dynamic_reconfigure.server

from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig

from sphero_class import sphero_start

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'



class swarm_start:
    def __init__(self):

        self.is_connected = False
        #Find nearby sphero 
        self.target_name = 'Sphero' #this allows for bluetooth to lookup local spheros
        self.name_list = []
        self.address_list = []
        self.color_topic_list = []
        self.vel_topic_list = []
        self.odom_topic_list = []
        self.imu_topic_list = []
        self.robot_list = []
        self.port_list = []
        self.sphero_dict = {} #to start the spheros/send data via. bluetooth
        self.call_color = False
        self.call_velocity = False
        self.call_imu = call_imu_odom = False
        self.connected_robots_list = []
        self.failed_robots_list = []
        self.robot_dict = {}


    def start(self,size, sphero_dictionary):
        sphero_begin = sphero_start()
        for i in range(1,size):        
            sphero_name = 'sphero%s' % (i)
            self.robot_dict[sphero_name] = sphero_start()
            self.robot_dict[sphero_name].initalize(sphero_dictionary[sphero_name])
        
def main(args):
    target_name = 'Sphero'
    nearby_devices = bluetooth.discover_devices(lookup_names = True)
    name_list = [] 
    address_list = []
    port_list = []
    color_topic_list = []
    vel_topic_list = []
    odom_topic_list = []
    imu_topic_list = []
    if len(nearby_devices)>0:
        for bdaddr, name in nearby_devices:
            #look for a device name that starts with Sphero
            if name.startswith(target_name):
                name_list.append(name)
                address_list.append(bdaddr)
                if len(name_list) <= 7:
                    port_list.append(1)  #Tell which bluetooth port to connect to
                elif len(name_list) > 7:
                    port_list.append(2)
            print('Devices to connect to:      ')
            print('name_list', name_list)
            print('address', address_list)
            print('I found %s nearby devices') % (len(nearby_devices)) 
            for i in range(0,len(name_list)):
                color_topic = 'sphero/color%s' % (i+1)
                vel_topic = 'sphero/velocity%s' % (i+1)
                odom_topic = 'sphero/odom%s' % (i+1)
                imu_topic = 'sphero/imu%s' % (i+1)
                color_topic_list.append(color_topic)
                vel_topic_list.append(vel_topic)
                odom_topic_list.append(odom_topic)
                imu_topic_list.append(imu_topic)
        connected_list = [False]*len(nearby_devices) #List which tell the actual robots which connected
    sphero_dictionary = {}
    for i in range(1,len(name_list)+1):
        print(i)
        sphero_name = 'sphero%s' % (i)
        color_topic = '/sphero/color%s' % (i)
        vel_topic = '/sphero/velocity%s' % (i)
        sphero_dictionary[sphero_name] = sphero_driver.Sphero(name_list[i-1],address_list[i-1], port_list[i-1])
    for i in range(1,len(name_list)+1):
        start = swarm_start()
        start.start(len(name_list)+1,sphero_dictionary)

    
        #sphero_dictionary[sphero_name].start(color_topic,vel_topic,sphero_name, name_list[i] ,address_list[i], port_list[i])


if __name__ == '__main__':
    rospy.init_node('robot', anonymous=True)
    #try:
        #main(sys.argv)
    while not rospy.is_shutdown():
        main(sys.argv)
        rospy.spin()
        
    #except KeyboardInterrupt:
        #print("Shutting down ROS Image feature detector module")

