#!/usr/bin/env python

#***********************************************************
#* Software License Agreement (BSD License)
#*
#*  Copyright (c) 2012, Melonee Wise
#*  All rights reserved.
#*
#*  Redistribution and use in source and binary forms, with or without
#*  modification, are permitted provided that the following conditions
#*  are met:
#*
#*   * Redistributions of source code must retain the above copyright
#*     notice, this list of conditions and the following disclaimer.
#*   * Redistributions in binary form must reproduce the above
#*     copyright notice, this list of conditions and the following
#*     disclaimer in the documentation and/or other materials provided
#*     with the distribution.
#*
#*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#*  POSSIBILITY OF SUCH DAMAGE.
#***********************************************************
#author: Mitchell Scott, with useage of sphero_ros work by Melonee Wise
import sys

import sys
import rospy
import math
import sys
import tf
import PyKDL
import bluetooth
import time




from sphero_driver import sphero_driver3 as sphero_driver
import dynamic_reconfigure.server

from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig

from updated_sphero_class import SpheroNode

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'


class start:
    def __init__(self):
        #Inital Information
        self.is_connected = False
        #Find nearby sphero 
        self.target_name = 'Sphero' #this allows for bluetooth to lookup local spheros
        self.name_list = []
        self.address_list = []
        self.color_topic_list = []
        self.vel_topic_list = []
        self.odom_topic_list = []
        self.imu_topic_list = []
        self.robot_list = []
        self.port_list = []
        self.sphero_dict = {} #to start the spheros/send data via. bluetooth
        self.robot_dict = {} #to subscribe/publish topics
        self.nearby_devices = bluetooth.discover_devices(lookup_names = True)
        print(self.nearby_devices)
        self.call_color = False
        self.call_velocity = False
        self.call_imu = call_imu_odom = False
        self.connected_robots_list = []
        self.failed_robots_list = []
        if len(self.nearby_devices)>0:
            for bdaddr, name in self.nearby_devices:
                #look for a device name that starts with Sphero
                if name.startswith(self.target_name):
                    self.name_list.append(name)
                    self.address_list.append(bdaddr)
                    if len(self.name_list) <= 7:
                        self.port_list.append(1)  #Tell which bluetooth port to connect to
                    elif len(self.name_list) > 7:
                        self.port_list.append(2)
            print('Devices to connect to:      ')
            print('name_list', self.name_list)
            print('address', self.address_list)
            print('I found %s nearby devices') % (len(self.nearby_devices)) 
            for i in range(0,len(self.name_list)):
                color_topic = 'sphero/color%s' % (i+1)
                vel_topic = 'sphero/velocity%s' % (i+1)
                odom_topic = 'sphero/odom%s' % (i+1)
                imu_topic = 'sphero/imu%s' % (i+1)
                self.color_topic_list.append(color_topic)
                self.vel_topic_list.append(vel_topic)
                self.odom_topic_list.append(odom_topic)
                self.imu_topic_list.append(imu_topic)
        self.connected_list = [False]*len(self.nearby_devices) #List which tell the actual robots which connected

    '''
    Tell the sphero drive the name address and port to connect to
    '''
    def begin(self):
        for i in range(0,len(self.name_list)):
            sphero_name = 'sphero%s' % (i+1)
            self.sphero_dict[sphero_name] = sphero_driver.Sphero(self.name_list[i], self.address_list[i], self.port_list[i])
        return True
    '''
    Connect the spheros and connect to topics
    '''
    def initalize(self):
        for i in range(0,len(self.name_list)):
            color_topic = 'sphero/color%s' % (i+1)
            vel_topic = 'sphero/vel%s' % (i+1)
            sphero_name = 'sphero%s' % (i+1)
            odom_topic = 'sphero/odom%s' % (i+1)
            imu_topic = 'sphero/imu%s' % (i+1)
            collision_topic = '/collision%s' % (i+1)
            diagnostic_topic = '/diagnostic%s' % (i+1)
            self.robot_list.append(sphero_name)
            self.robot_dict[sphero_name] = sphero_start() 
            self.is_connected = self.robot_dict[sphero_name].connect(self.sphero_dict[sphero_name])
            if self.is_connected:
                self.connected_list[i] = True #if it connected, set to true
                print('%s is connected') % (sphero_name)
                self.connected_robots_list.append(sphero_name)
            else:
                self.failed_robots_list.append(sphero_name)

        topic = self.topic_start(self.connected_list)
        if topic:
            return True
        
    def topic_start(self,connected_list):
        self.connected_list = connected_list
        self.new_robot_list = [j for i,j in enumerate(self.robot_list) if self.connected_list[i] == True]
        for i in range(0,len(self.new_robot_list)): 
            sphero_name = self.new_robot_list[i] 
            color_topic = 'sphero/color%s' % (i+1)
            vel_topic = 'sphero/vel%s' % (i+1)
            init = self.robot_dict[sphero_name].initalize(i)
            if init:
                return True


if __name__ == '__main__':
    print('searching')
    rospy.init_node('sphero', anonymous=True)
    target_name = 'Sphero'
    nearby_devices = bluetooth.discover_devices(lookup_names = True)
    print(nearby_devices)
    call_color = False
    call_velocity = False
    call_imu = call_imu_odom = False
    name_list = []
    address_list = []
    port_list = []
    connected_robots_list = []
    failed_robots_list = []
    if len(nearby_devices)>0:
        for bdaddr, name in nearby_devices:
             #look for a device name that starts with Sphero
              if name.startswith(target_name):
                  name_list.append(name)
                  address_list.append(bdaddr)
                  if len(name_list) <= 7:
                       port_list.append(1)  #Tell which bluetooth port to connect to
                  elif len(name_list) > 7:
                      port_list.append(2)
        print('Devices to connect to:      ')
        print('name_list', name_list)
        print('address', address_list)
        print('I found %s nearby devices') % (len(nearby_devices)) 
    connected_list = [False]*len(nearby_devices) #List which tell the actual robots which connected

    for i in range(0,len(name_list)):
        node_name = 'sphero%s' % (i+1)
        #color_topic = 'sphero%s/color' % (i+1)
        #vel_topic = 'sphero%s/velocity' % (i+1)
        #odom_topic = 'sphero%s/odom' % (i+1)
        #imu_topic = 'sphero%s/imu' % (i+1)
        start = SpheroNode(name_list[i],address_list[i],port_list[i],node_name,i+1)
        on = start.start()
"""
    try:
        print("Connecting ... be patient")
        inital_start = start.initalize()

    except KeyboardInterrupt:
        start.stop(start.connected_robots_list)
        print(color.RED + 'Failure. Exciting. Check bluettoth connection, try again.' + color.END)
        sys.exit(1)

    if inital_start:
        print('Connected to the following spheros:', start.connected_robots_list)
        time.sleep(2)
        if len(start.failed_robots_list) >=1:
            print('Failed to connect to the following spheros:', start.failed_robots_list)
        time.sleep(2)
        print(color.GREEN + 'Everything is great! Go Cougs! You may move the spheros now.' + color.END)
    rospy.spin()

"""













