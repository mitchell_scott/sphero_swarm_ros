from sphero_driver import sphero_driver2 as sphero_driver
import sys
import struct
import time
import operator
import threading
import rospy

MRSP = dict(
  ORBOTIX_RSP_CODE_OK = 0x00,           #Command succeeded
  ORBOTIX_RSP_CODE_EGEN = 0x01,         #General, non-specific error
  ORBOTIX_RSP_CODE_ECHKSUM = 0x02,      #Received checksum failure
  ORBOTIX_RSP_CODE_EFRAG = 0x03,        #Received command fragment
  ORBOTIX_RSP_CODE_EBAD_CMD = 0x04,     #Unknown command ID
  ORBOTIX_RSP_CODE_EUNSUPP = 0x05,      #Command currently unsupported
  ORBOTIX_RSP_CODE_EBAD_MSG = 0x06,     #Bad message format
  ORBOTIX_RSP_CODE_EPARAM = 0x07,       #Parameter value(s) invalid
  ORBOTIX_RSP_CODE_EEXEC = 0x08,        #Failed to execute command
  ORBOTIX_RSP_CODE_EBAD_DID = 0x09,     #Unknown device ID
  ORBOTIX_RSP_CODE_POWER_NOGOOD = 0x31, #Voltage too low for refash operation
  ORBOTIX_RSP_CODE_PAGE_ILLEGAL = 0x32, #Illegal page number provided
  ORBOTIX_RSP_CODE_FLASH_FAIL = 0x33,   #Page did not reprogram correctly
  ORBOTIX_RSP_CODE_MA_CORRUPT = 0x34,   #Main application corrupt
  ORBOTIX_RSP_CODE_MSG_TIMEOUT = 0x35)  #Msg state machine timed out


#ID codes for asynchronous packets
IDCODE = dict(
  PWR_NOTIFY = chr(0x01),               #Power notifications
  LEVEL1_DIAG = chr(0x02),              #Level 1 Diagnostic response
  DATA_STRM = chr(0x03),                #Sensor data streaming
  CONFIG_BLOCK = chr(0x04),             #Config block contents
  SLEEP = chr(0x05),                    #Pre-sleep warning (10 sec)
  MACRO_MARKERS =chr(0x06),             #Macro markers
  COLLISION = chr(0x07))                #Collision detected

RECV = dict(
  ASYNC = [chr(0xff), chr(0xfe)],
  SYNC = [chr(0xff), chr(0xff)])


REQ = dict(
  WITH_RESPONSE =[0xff, 0xff],
  WITHOUT_RESPONSE =[0xff, 0xfe],
  CMD_PING = [0x00, 0x01],
  CMD_VERSION = [0x00, 0x02],
  CMD_SET_BT_NAME = [0x00, 0x10],
  CMD_GET_BT_NAME = [0x00, 0x11],
  CMD_SET_AUTO_RECONNECT = [0x00, 0x12],
  CMD_GET_AUTO_RECONNECT = [0x00, 0x13],
  CMD_GET_PWR_STATE = [0x00, 0x20],
  CMD_SET_PWR_NOTIFY = [0x00, 0x21],
  CMD_SLEEP = [0x00, 0x22],
  CMD_GOTO_BL = [0x00, 0x30],
  CMD_RUN_L1_DIAGS = [0x00, 0x40],
  CMD_RUN_L2_DIAGS = [0x00, 0x41],
  CMD_CLEAR_COUNTERS = [0x00, 0x42],
  CMD_ASSIGN_COUNTER = [0x00, 0x50],
  CMD_POLL_TIMES = [0x00, 0x51],
  CMD_SET_HEADING = [0x02, 0x01],
  CMD_SET_STABILIZ = [0x02, 0x02],
  CMD_SET_ROTATION_RATE = [0x02, 0x03],
  CMD_SET_APP_CONFIG_BLK = [0x02, 0x04],
  CMD_GET_APP_CONFIG_BLK = [0x02, 0x05],
  CMD_SET_DATA_STRM = [0x02, 0x11],
  CMD_CFG_COL_DET = [0x02, 0x12],
  CMD_SET_RGB_LED = [0x02, 0x20],
  CMD_SET_BACK_LED = [0x02, 0x21],
  CMD_GET_RGB_LED = [0x02, 0x22],
  CMD_ROLL = [0x02, 0x30],
  CMD_BOOST = [0x02, 0x31],
  CMD_SET_RAW_MOTORS = [0x02, 0x33],
  CMD_SET_MOTION_TO = [0x02, 0x34],
  CMD_GET_CONFIG_BLK = [0x02, 0x40],
  CMD_SET_DEVICE_MODE = [0x02, 0x42],
  CMD_SET_CFG_BLOCK = [0x02, 0x43],
  CMD_GET_DEVICE_MODE = [0x02, 0x44],
  CMD_RUN_MACRO = [0x02, 0x50],
  CMD_SAVE_TEMP_MACRO = [0x02, 0x51],
  CMD_SAVE_MACRO = [0x02, 0x52],
  CMD_DEL_MACRO = [0x02, 0x53],
  CMD_INIT_MACRO_EXECUTIVE = [0x02, 0x54],
  CMD_ABORT_MACRO = [0x02, 0x55],
  CMD_GET_MACRO_STATUS = [0x02, 0x56],
  CMD_SET_MACRO_STATUS = [0x02, 0x57])

STRM_MASK1 = dict(
  GYRO_H_FILTERED    = 0x00000001,
  GYRO_M_FILTERED    = 0x00000002,
  GYRO_L_FILTERED    = 0x00000004,
  LEFT_EMF_FILTERED  = 0x00000020,
  RIGHT_EMF_FILTERED = 0x00000040,
  MAG_Z_FILTERED     = 0x00000080,
  MAG_Y_FILTERED     = 0x00000100,
  MAG_X_FILTERED     = 0x00000200,
  GYRO_Z_FILTERED    = 0x00000400,
  GYRO_Y_FILTERED    = 0x00000800,
  GYRO_X_FILTERED    = 0x00001000,
  ACCEL_Z_FILTERED   = 0x00002000,
  ACCEL_Y_FILTERED   = 0x00004000,
  ACCEL_X_FILTERED   = 0x00008000,
  IMU_YAW_FILTERED   = 0x00010000,
  IMU_ROLL_FILTERED  = 0x00020000,
  IMU_PITCH_FILTERED = 0x00040000,
  LEFT_EMF_RAW       = 0x00200000,
  RIGHT_EMF_RAW      = 0x00400000,
  MAG_Z_RAW          = 0x00800000,
  MAG_Y_RAW          = 0x01000000,
  MAG_X_RAW          = 0x02000000,
  GYRO_Z_RAW         = 0x04000000,
  GYRO_Y_RAW         = 0x08000000,
  GYRO_X_RAW         = 0x10000000,
  ACCEL_Z_RAW        = 0x20000000,
  ACCEL_Y_RAW        = 0x40000000,
  ACCEL_X_RAW        = 0x80000000)

STRM_MASK2 = dict(
  QUATERNION_Q0      = 0x80000000,
  QUATERNION_Q1      = 0x40000000,
  QUATERNION_Q2      = 0x20000000,
  QUATERNION_Q3      = 0x10000000,
  ODOM_X             = 0x08000000,
  ODOM_Y             = 0x04000000,
  ACCELONE           = 0x02000000,
  VELOCITY_X         = 0x01000000,
  VELOCITY_Y         = 0x00800000)


class many_streaming:

    def __init__(self):
        self.raw_data_buf2 = []
        self.bluetooth_dict = {}
        self.data_dict = {}
        self.in_loop_data = {}
        self.data_packet = {}
        self.data_length = {}
        self.run = True
        self.is_connected = True
        self.shutdown = False
        self._communication_lock = threading.Lock()


    def organize(self, connected_list, connected_robot_dictionary):
        self.robot_dictionary = connected_robot_dictionary
        key_list = list(self.robot_dictionary.keys())
        #print(robot_dictionary)
        for i in range(0, len(key_list)):
            sphero_name = key_list[i]
            self.data_dict[sphero_name] = []
            self.bluetooth_dict[sphero_name] = self.robot_dictionary[sphero_name].robot.bt
        self.btrun(len(key_list),key_list)


    def btrun(self, sphero_number, key_list):
        i = 1
        while not rospy.is_shutdown():
            if i <= sphero_number:
                number = i 
            elif i > sphero_number:
                number = i % sphero_number 
                if number == 0:
                    number = sphero_number
            #print('number of connected', sphero_number)
            #print('i', i) 
            #print('num', number)
            i = i + 1
            #for i in range(0,sphero_number):
            #print(key_list)
            sphero_name = key_list[number-1]
            '''
            Check connection to see if still running
            '''
            #connection = self.robot_dictionary[sphero_name].robot.check_connection()
            #if connection:
            with self._communication_lock:
                    self.data_dict[sphero_name] += self.bluetooth_dict[sphero_name].recv(1024)
            data = self.data_dict[sphero_name]
            #print(sphero_name)
            #if data[:2]  == RECV['SYNC'] or data[:2] == RECV['ASYNC']:
            if len(data) > 5:
              while len(data) > 5:
                #print('length > 5', sphero_name)
                #print(number, data)
                if data[:2] == RECV['SYNC']:
                    #print "got response packet"
                    # response packet
                    data_length = ord(data[4])
                    if data_length+5 <= len(data):
                        data_packet = data[:(5+data_length)]
                        data = data[(5+data_length):]
                    else:
                        break
                        #print "Response packet", self.data2hexstr(data_packet)
         
                elif data[:2] == RECV['ASYNC']:
                    data_length = (ord(data[3])<<8)+ord(data[4])
                    if data_length+5 <= len(data):
                        data_packet = data[:(5+data_length)]
                        data = data[(5+data_length):]
                    else:
                        # the remainder of the packet isn't long enough
                        break
                    if data_packet[2]==IDCODE['DATA_STRM'] and self.robot_dictionary[sphero_name].robot._async_callback_dict.has_key(IDCODE['DATA_STRM']):
                        #print('Data', sphero_name)
                        self.robot_dictionary[sphero_name].robot._async_callback_dict[IDCODE['DATA_STRM']](self.robot_dictionary[sphero_name].robot.parse_data_strm(data_packet, data_length))
                    elif data_packet[2]==IDCODE['COLLISION'] and self.robot_dictionary[sphero_name].robot._async_callback_dict.has_key(IDCODE['COLLISION']):
                        #print('Collision', sphero_name)
                        self.robot_dictionary[sphero_name].robot._async_callback_dict[IDCODE['COLLISION']](self.robot_dictionary[sphero_name].robot.parse_collision_detect(data_packet, data_length))
                    elif data_packet[2]==IDCODE['PWR_NOTIFY'] and self.robot_dictionary[sphero_name].robot._async_callback_dict.has_key(IDCODE['PWR_NOTIFY']):
                        #print('Power', sphero_name)
                        self.robot_dictionary[sphero_name].robot._async_callback_dict[IDCODE['PWR_NOTIFY']](self.robot_dictionary[sphero_name].robot.parse_pwr_notify(data_packet, data_length))
                    #else:
                        #   raise RuntimeError("Bad SOF : " + self.data2hexstr(data))
                else:
                    data = []
                    self.data_dict[sphero_name] = []
                    break
                self.data_dict[sphero_name] = data
           



            """ 
                         if self.in_loop_data[sphero_name][:2] == RECV['SYNC'] and len(self.in_loop_data[sphero_name])>5:
                             print('Sync', sphero_name)
                             #print "got response packet"
                             # response packet
                             self.data_length[sphero_name] = ord(self.in_loop_data[sphero_name][4])
                             if self.data_length[sphero_name]+5 <= len(self.in_loop_data[sphero_name]):
                                 self.data_packet[sphero_name] = self.in_loop_data[sphero_name][:(5+self.data_length[sphero_name])]
                                 self.in_loop_data[sphero_name] = self.in_loop_data[sphero_name][(5+self.data_length[sphero_name]):]
                              #else:
                                 #break
                                 #print "Response packet", self.data2hexstr(data_packet)                    


                     else:
                         print('pass', sphero_name)
                         self.data_dict[sphero_name] = []
                         pass  
                         """


    def run_data(self, sphero_name, data):
        print('data')
        if data[:2] == RECV['SYNC']:
          #print "got response packet"
          # response packet
          data_length = ord(data[4])
          if data_length+5 <= len(data):
            data_packet = data[:(5+data_length)]
            data = data[(5+data_length):]
          #else:
            #break
            #print "Response packet", self.data2hexstr(data_packet)

        elif data[:2] == RECV['ASYNC']:
          data_length = (ord(data[3])<<8)+ord(data[4])
          if data_length+5 <= len(data):
            data_packet = data[:(5+data_length)]
            data = data[(5+data_length):]
          #else:
            # the remainder of the packet isn't long enough
            #break
          if data_packet[2]==IDCODE['DATA_STRM'] and self.robot_dictionary[sphero_name].robot._async_callback_dict.has_key(IDCODE['DATA_STRM']):
            print('Data', sphero_name)
            #self._async_callback_dict[IDCODE['DATA_STRM']](self.parse_data_strm(data_packet, data_length))
          elif data_packet[2]==IDCODE['COLLISION'] and self.robot_dictionary[sphero_name].robot._async_callback_dict.has_key(IDCODE['COLLISION']):
            print('Collision', sphero_name)
            #self._async_callback_dict[IDCODE['COLLISION']](self.parse_collision_detect(data_packet, data_length))
          elif data_packet[2]==IDCODE['PWR_NOTIFY'] and self.robot_dictionary[sphero_name].robot._async_callback_dict.has_key(IDCODE['PWR_NOTIFY']):
            print('Power', sphero_name)
            #self._async_callback_dict[IDCODE['PWR_NOTIFY']](self.parse_pwr_notify(data_packet, data_length))
          else:
            print "got a packet that isn't streaming: " + self.data2hexstr(data)
        else:
          raise RuntimeError("Bad SOF : " + self.data2hexstr(data))
      #self.raw_data_buf=data


    def data2hexstr(self, data):
      return ' '.join([ ("%02x"%ord(d)) for d in data])


    """
            #with self._communication_lock:
             for i in range(0,sphero_number):
                 #print(sphero_number)
                 sphero_name = 'sphero%s' % (i+1)
                 print(sphero_name)
                 self.data_dict[sphero_name] += self.bluetooth_dict[sphero_name].recv(1024)
                 #while self.data_dict[sphero_name][:2] != RECV['SYNC'] or self.data_dict[sphero_name][:2] != RECV['SYNC']:
                     #self.data_dict[sphero_name] = []
                     #self.data_dict[sphero_name] += self.bluetooth_dict[sphero_name].recv(1024)  
                     #print('trying')
                 print(self.data_dict[sphero_name][:2])
                 print(self.data_dict[sphero_name][:2] == RECV['ASYNC'])
                 self.in_loop_data[sphero_name] = self.data_dict[sphero_name]
                 if self.data_dict[sphero_name][:2] == RECV['ASYNC'] or self.data_dict[sphero_name][:2]== RECV['SYNC']:
                     print('success', sphero_name)         
                     self.in_loop_data[sphero_name] = self.data_dict[sphero_name]
                        
                     print('true1',sphero_name)
                     if self.in_loop_data[sphero_name][:2] == RECV['SYNC'] and len(self.in_loop_data[sphero_name])>5:
                         print('Sync', sphero_name)
                         #print "got response packet"
                         # response packet
                         self.data_length[sphero_name] = ord(self.in_loop_data[sphero_name][4])
                         if self.data_length[sphero_name]+5 <= len(self.in_loop_data[sphero_name]):
                             self.data_packet[sphero_name] = self.in_loop_data[sphero_name][:(5+self.data_length[sphero_name])]
                             self.in_loop_data[sphero_name] = self.in_loop_data[sphero_name][(5+self.data_length[sphero_name]):]
                         #else:
                             #break
                             #print "Response packet", self.data2hexstr(data_packet)


                     elif self.in_loop_data[sphero_name][:2] == RECV['ASYNC'] and len(self.in_loop_data[sphero_name])>5:
                         print('ASYNC', sphero_name)
                         self.data_length[sphero_name] = (ord(self.in_loop_data[sphero_name][3])<<8)+ord(self.in_loop_data[sphero_name][4])
                         if self.data_length[sphero_name]+5 <= len(self.in_loop_data[sphero_name]):
                             self.data_packet[sphero_name] = self.data_dict[sphero_name][:(5+self.data_length[sphero_name])]
                             self.in_loop_data[sphero_name] = self.in_loop_data[sphero_name][(5+self.data_length[sphero_name]):]
                         #else:
                             # the remainder of the packet isn't long enough
                             #break
                         if self.data_packet[sphero_name][2]==IDCODE['DATA_STRM'] and self.robot_dictionary[sphero_name].robot._async_callback_dict.has_key(IDCODE['DATA_STRM']):
                            print('Stream', sphero_name)
                            self.robot_dictionary[sphero_name].robot._async_callback_dict[IDCODE['DATA_STRM']](self.robot_dictionary[sphero_name].robot.parse_data_strm(self.data_packet[sphero_name], self.data_length[sphero_name]))
                         elif self.data_packet[2]==IDCODE['COLLISION'] and self.robot_dictionary[sphero_name].robot._async_callback_dict.has_key(IDCODE['COLLISION']):
                             print('Collision', sphero_name)
                             self.robot_dictionary[sphero_name].robot._async_callback_dict[IDCODE['COLLISION']](self.robot_dictionary[sphero_name].robot.parse_collision_detect(self.data_packet[sphero_name], self.data_length[sphero_name]))
                         elif self.data_packet[2]==IDCODE['PWR_NOTIFY'] and self.robot_dictionary[sphero_name].robot._async_callback_dict.has_key(IDCODE['PWR_NOTIFY']):
                             self.robot_dictionary[sphero_name].robot._async_callback_dict[IDCODE['PWR_NOTIFY']](self.robot_dictionary[sphero_name].robot.parse_pwr_notify(self.data_packet[sphero_name], self.data_length[sphero_name]))
                             print('Power', sphero_name)
                 else: 
                     print('else')
                     pass
                 self.data_dict[sphero_name] = data
                 """
    """
    def parse_pwr_notify(self, data, data_length):
        '''
        The data payload of the async message is 1h bytes long and
        formatted as follows::

          --------
          |State |
          --------

        The power state byte: 
          * 01h = Battery Charging, 
          * 02h = Battery OK,
          * 03h = Battery Low, 
          * 04h = Battery Critical
        '''
        return struct.unpack_from('B', ''.join(data[5:]))[0]

    def parse_collision_detect(self, data, data_length):
        '''
        The data payload of the async message is 10h bytes long and
        formatted as follows::

          -----------------------------------------------------------------
          |X | Y | Z | AXIS | xMagnitude | yMagnitude | Speed | Timestamp |
          -----------------------------------------------------------------

        * X, Y, Z - Impact components normalized as a signed 16-bit\
        value. Use these to determine the direction of collision event. If\
        you don't require this level of fidelity, the two Magnitude fields\
        encapsulate the same data in pre-processed format.
        * Axis - This bitfield specifies which axes had their trigger\
        thresholds exceeded to generate the event. Bit 0 (01h) signifies\
        the X axis and bit 1 (02h) the Y axis.
        * xMagnitude - This is the power that crossed the programming\
        threshold Xt + Xs.
        * yMagnitude - This is the power that crossed the programming\
        threshold Yt + Ys.
        * Speed - The speed of Sphero when the impact was detected.
        * Timestamp - The millisecond timer value at the time of impact;\
        refer to the documentation of CID 50h and 51h to make sense of\
        this value.
        '''
        output={}
    
        output['X'], output['Y'], output['Z'], output['Axis'], output['xMagnitude'], output['yMagnitude'], output['Speed'], output['Timestamp'] = struct.unpack_from('>hhhbhhbI', ''.join(data[5:]))
        return output

    def parse_data_strm(self, data, data_length):
        output={}
        for i in range((data_length-1)/2):
            unpack = struct.unpack_from('>h', ''.join(data[5+2*i:]))
            output[self.mask_list[i]] = unpack[0]
        #print self.mask_list
        #print output
        return output
   """





