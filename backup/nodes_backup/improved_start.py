#!/usr/bin/env python

#***********************************************************
#* Software License Agreement (BSD License)
#*
#*  Copyright (c) 2012, Melonee Wise
#*  All rights reserved.
#*
#*  Redistribution and use in source and binary forms, with or without
#*  modification, are permitted provided that the following conditions
#*  are met:
#*
#*   * Redistributions of source code must retain the above copyright
#*     notice, this list of conditions and the following disclaimer.
#*   * Redistributions in binary form must reproduce the above
#*     copyright notice, this list of conditions and the following
#*     disclaimer in the documentation and/or other materials provided
#*     with the distribution.
#*
#*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#*  POSSIBILITY OF SUCH DAMAGE.
#***********************************************************
#author: Mitchell Scott, with useage of sphero_ros work by Melonee Wise
import sys

import sys
import rospy
import math
import sys
import tf
import PyKDL
import bluetooth
import time
from bluetooth_streaming import many_streaming


from sphero_driver import sphero_driver2 as sphero_driver
import dynamic_reconfigure.server

from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig

from sphero_class import sphero_start



class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'


class start:
    def __init__(self):
        #Inital Information
        self.is_connected = False
        #Find nearby sphero 
        self.target_name = 'Sphero' #this allows for bluetooth to lookup local spheros
        self.name_list = []
        self.address_list = []
        self.color_topic_list = []
        self.vel_topic_list = []
        self.odom_topic_list = []
        self.imu_topic_list = []
        self.robot_list = []
        self.port_list = []
        self.sphero_dict = {} #to start the spheros/send data via. bluetooth
        self.robot_dict = {} #to subscribe/publish topics
        self.connected_dictionary = {}
        self.nearby_devices = bluetooth.discover_devices(lookup_names = True)
        print(self.nearby_devices)
        self.call_color = False
        self.call_velocity = False
        self.call_imu = call_imu_odom = False
        self.connected_robots_list = []
        self.failed_robots_list = []
        if len(self.nearby_devices)>0:
            for bdaddr, name in self.nearby_devices:
                #look for a device name that starts with Sphero
                if name.startswith(self.target_name):
                    #if name != 'Sphero-RRY' and name != 'Sphero-BWG' and name != 'Sphero-BPR' and name != 'Sphero-ROY' and name != 'Sphero-BRY' and name != 'Sphero-WWB' and name != 'Sphero-OPY':
                    #if name == 'Sphero-WBR' or name == 'Sphero-ORY' or name == 'Sphero-PRY' or name == 'Sphero-RRB' or name == 'Sphero-BBR' or name == 'Sphero-WRP' or name == 'Sphero-GRR' or name == 'Sphero-GRW' or name == 'Sphero-BOP' or name == 'Sphero-WWB' or name == 'Sphero-WBR':
                    self.name_list.append(name)
                    self.address_list.append(bdaddr)
                    if len(self.name_list) <= 7:
                        self.port_list.append(1)  #Tell which bluetooth port to connect to
                    elif len(self.name_list) > 7:
                       self.port_list.append(2)
            print('Devices to connect to:      ')
            print('name_list', self.name_list)
            print('address', self.address_list)
            for i in range(0,len(self.name_list)):
                color_topic = 'sphero/color%s' % (i+1)
                vel_topic = 'sphero/velocity%s' % (i+1)
                odom_topic = 'sphero/odom%s' % (i+1)
                imu_topic = 'sphero/imu%s' % (i+1)
                self.color_topic_list.append(color_topic)
                self.vel_topic_list.append(vel_topic)
                self.odom_topic_list.append(odom_topic)
                self.imu_topic_list.append(imu_topic)
        self.connected_list = [False]*len(self.nearby_devices) #List which tell the actual robots which connected

    def begin(self):
        '''
        Tell the sphero drive the name address and port to connect to
        '''
        for i in range(0,len(self.name_list)):
            sphero_name = 'sphero%s' % (i+1)
            self.sphero_dict[sphero_name] = sphero_driver.Sphero(i+1, self.name_list[i], self.address_list[i], self.port_list[i])
        return True

    def initalize(self):
        '''
        Try to connect the spheros. If successful, it will initalize the subscribers and record the connected robots to a dictionary 
        '''
        for i in range(0,len(self.name_list)):
            color_topic = 'sphero/color%s' % (i+1)
            vel_topic = 'sphero/vel%s' % (i+1)
            sphero_name = 'sphero%s' % (i+1)
            odom_topic = 'sphero/odom%s' % (i+1)
            imu_topic = 'sphero/imu%s' % (i+1)
            self.robot_list.append(sphero_name)
            self.robot_dict[sphero_name] = sphero_start()
            initalize= self.robot_dict[sphero_name].initalize(i+1, self.sphero_dict[sphero_name]) #Must run for the spheros to actually connect via. bluetooth
           
            if initalize:
               self.connected_list[i] = True #if it connected, set to true


    def odom_imu(self,connected_list):
        '''
        Takes in the list of connected robots. If the robots are connected, initalize the odom and imu publishers
        '''
        self.new_robot_list = [j for i,j in enumerate(self.robot_list) if self.connected_list[i] == True]
        for i in range(0,len(self.new_robot_list)): 
            odom_topic = 'sphero/odom%s' % (i+1)
            imu_topic = 'sphero/imu%s' % (i+1)
            diag_topic = 'sphero/diag%s' % (i+1)
            sphero_name = self.new_robot_list[i] 
            self.call_imu =self.robot_dict[sphero_name].call_odom_imu(i+1,len(self.new_robot_list))



    def data_run(self):
        '''
        Start reciving bluetooth data and publish imu and odom messages for ROS. Tells which robots are connected and which to stream data from
        '''
        for i in range(0,len(self.connected_list)):
            sphero_name = 'sphero%s' % (i+1)
            print(self.connected_list)
            if self.connected_list[i] == True:
                self.connected_dictionary[sphero_name] = self.robot_dict[sphero_name]
        organize = many_streaming()
        
        organize.organize(self.connected_list, self.connected_dictionary)          

if __name__ == '__main__':
    print("Searching")
    rospy.init_node('sphero')
    start = start()
    print("Connecting ... be patient")
    start.begin()
    inital_start = start.initalize()
    start.odom_imu(start.connected_robots_list)
    start.data_run()
























