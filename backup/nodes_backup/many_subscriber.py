#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped, Pose, Twist
from sensor_msgs.msg import Imu
import numpy as np
import math
import sys
import time


import dynamic_reconfigure.server
from sphero_driver import sphero_driver2 as sphero_driver
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig


class robot():

    def __init__(self):
        self.robot_number = 1
        self.x_data = 0
        self.y_data = 0
        self.z = 0
        self.x_vel = 0
        self.y_vel = 0
        self.position = []
        self.time_inital = rospy.get_time()
    def main(self,imu_topic,velocity_topic,robot_name):
        self.robot_name = robot_name
        rospy.Subscriber(imu_topic, Imu, self.callback) 
        self.pose_pub = rospy.Publisher(velocity_topic,Twist, queue_size = 10)

    def callback(self,data):
        speed = 30
        print(data)
        frequency = math.pi
        twist = Twist()
        current_time = rospy.get_time()
        move_time = time.time() - self.time_inital
        x_vel = speed*math.cos(frequency*move_time)
        y_vel = speed*math.sin(frequency*move_time)
        twist.linear.x = x_vel
        twist.linear.y = y_vel
        self.pose_pub.publish(twist)





def main(args):
    '''Initializes and cleanup ros node'''
    robot_number = 1
    robot_dictionary = {}
    robot_position = {}
    robot_velocity = {}
    for i in range(1,robot_number+1):
        robot_name = 'robot%s' % (i)
        print(robot_number)
        robot_dictionary[robot_name] = robot()
        imu_topic = '/sphero/imu%s' % (i)
        vel_topic = '/sphero/vel%s' % (i)
        robot_dictionary[robot_name].main(imu_topic,vel_topic,robot_name)



if __name__ == '__main__':
    rospy.init_node('robot', anonymous=True)
    #try:
        #main(sys.argv)
    while not rospy.is_shutdown():
        main(sys.argv)
        rospy.spin()
        
    #except KeyboardInterrupt:
        #print("Shutting down ROS Image feature detector module")
