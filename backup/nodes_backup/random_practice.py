#!/usr/bin/python
from sphero_driver import sphero_driver
import time
import rospy
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
import bluetooth
import sys

rospy.init_node('sphero2')
"""
sphero1 = sphero_driver.Sphero('Sphero','68:86:E7:08:99:C3', 1)
sphero2 = sphero_driver.Sphero('Sphero','68:86:E7:08:AD:B5', 2)

a = sphero1.connect()
#b = sphero2.connect()

def set_color1(msg):
    sphero1.set_rgb_led(int(msg.r*255),int(msg.g*255),int(msg.b*255),0,False)

#def set_color2(msg):
#    sphero2.set_rgb_led(int(msg.r*255),int(msg.g*255),int(msg.b*255),0,False)


if a == True:# and b== True:
    print("Sphero1 and Sphero2")
    b = sphero2.connect()
    color_sub1 = rospy.Subscriber('set_color1', ColorRGBA, set_color1, queue_size = 1)
    #color_sub2 = rospy.Subscriber('set_color2', ColorRGBA, set_color2, queue_size = 1)
#if a == True and b != True:
#    print("Sphero1")
#if b == True and a != True:
#    print("Sphero2")

#if a == True and b == True:
#    print('Both')




rospy.spin()
"""

address1 = '68:86:E7:08:99:C3'
address2 = '68:86:E7:08:AD:B5'
sock1=bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock2=bluetooth.BluetoothSocket(bluetooth.RFCOMM)

try:
    #sock1.connect((address1,1))  
    sock2.connect((address2,2))
except:
    print("error")
    sys.exit(1)

rospy.spin()
