Sphero_ros (based on the work of Melonee Wise) (http://mmwise.github.com/sphero_ros) for multiple spheros
==========

## SETUP:


NOTE: MUST USE PYTHON2.7 Will be denoted as python here
To get clone into catkin_ws/src run catkin_make and source  
1. $ cd <ws>/src  
2. $ git clone <name>  
3. $ cd ~<ws>  
4. $ catkin_make  
5. $ echo "source ~/<ws>/devel/setup.bash" >> ~/.bashrc  

Any errors? $ source ~/<ws>/devel/setup.bash on all new terminals  

##USAGE:

To conntect to one sphero:  

1. $ cd <ws>/src/sphero_swarm_ros/sphero_node/nodes  
2. $ roscore  
3. $ python sphero.py (on new terminal). This starts the node which actually connect to the one sphero.   
4. $ rostopic list to view the topics running.  
Use ros to control the spheros.   


Multiple spheros:  

1. $ cd <ws>/src/sphero_swarm_ros/sphero_node/nodes  
2. $ roscore  
3. $ python improved_sphero_async_start.py (on new terminal). This starts the node which actually connects to all the spheros in the area.   
Optional:
4. $ python move_spheros.py (on new terminal). This will cause all robots to spin in a circle.  



![Screenshot](Improved_diagram.png "Diagram")  



##How the Multi-threading works
The python script improved_sphero_async_start.py will locate all the spheros in the area. It will then inialize a sphero_start() class (from sphero_class) for each of the spheros. This will allow the spheros to subscribe and publish to all proper topics named as /<topic>number (/odom1 for example).  
The sphero_start() class uses the sphero_driver3.py scipt to actuall connect to the spheros. The sphero_driver() class accepts the name, address, and port for each sphero to connect to.   
Before connecting, the node will split into 'n' different threads (the size of the number of robots it will connect to) and then connect to each sphero.  
This class will also recieve and send bluetooth data to the spheros.  
Once the spheros are connected, the connected sphero instances are sent to the script multi_streaming.py which will run through each blueooth connection to recieve data from the sphero. It will then publish the proper data through ros topics.  

