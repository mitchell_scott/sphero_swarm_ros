import bluetooth
import asyncore

class connect(asyncore.dispatcher):
    def __init__(self, address, port):
        asyncore.dispatcher.__init__(self)
        self.connect(address)


nearby_devices = bluetooth.discover_devices(duration=20, lookup_names = True)
target_name = 'Sphero'
port_list = []
name_list = []
address_list = []
if len(nearby_devices)>0:
    for bdaddr, name in nearby_devices:
        if name.startswith(target_name):
            '''
            Append names and address of all the bluetooth devices in the area that start with the bame 'Sphero'.
            '''
            #print('no pass')
            name_list.append(name)
            address_list.append(bdaddr)
            port_list.append(1)
            
    
connect(address_list[0], 1)
asyncore.loop()