#tf = transformer
import sys, rospy, math, tf, PyKDL, bluetooth
import dynamic_reconfigure.server

from sphero_driver import sphero_driver2 as sphero_driver
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig

from sphero_class import sphero_start

#Inital Information
is_connected = False
robot_number = 2
#Find nearby sphero 
target_name = 'Sphero' #this allows for bluetooth to lookup local spheros
name_list = []
address_list = []
color_topic_list = []
vel_topic_list = []
odom_topic_list = []
imu_topic_list = []
sphero_dict = {} #to start the spheros/send data via. bluetooth
robot_dict = {} #to subscribe/publish topics

nearby_devices = bluetooth.discover_devices(lookup_names = True)
print(nearby_devices)
if len(nearby_devices)>0:
    for bdaddr, name in nearby_devices:
        #look for a device name that starts with Sphero
        if name.startswith(target_name):
            name_list.append(name)
            address_list.append(bdaddr)
print('Devices to connect to:      ')
print('name_list',name_list)
print('address', address_list)
for i in range(0,len(name_list)):
    color_topic = 'sphero/color%s' % (i+1)
    vel_topic = 'sphero/velocity%s' % (i+1)
    odom_topic = 'sphero/odom%s' % (i+1)
    imu_topic = 'sphero/imu%s' % (i+1)
    color_topic_list.append(color_topic)
    vel_topic_list.append(vel_topic)
    odom_topic_list.append(odom_topic)
    imu_topic_list.append(imu_topic)
'''
Create instances of the sphero 
'''
def begin(name_list):
    for i in range(0,len(name_list)):
        sphero_name = 'sphero%s' % (i+1)
        print(sphero_name)
        sphero_dict[sphero_name] = sphero_driver.Sphero(name_list[i], address_list[i], 1)

def topic_start(color_topic_list,vel_topic_list,odom_topic_list,imu_topic_list):
    for i in range(0,len(color_topic_list)):
        sphero_name = 'sphero%s' % (i+1)
        robot_dict[sphero_name] = sphero_start() #With the numbr of spheros defined by the sphero class, we can now initalzie topics for each of the spheros
        initalize = robot_dict[sphero_name].initalize(sphero_dict[sphero_name]) #Must run for the spheros to actually connect via. bluetooth
        robot_dict[sphero_name].call_color(color_topic_list[i]) #subscriber. User can publish to /sphero/color# in RGBA message type to change colors
        '''
        NOTE: call_velocity publication is in bits/255 with a max speed of 2.5 m/s. So a command of 50 is equal to 50/255 bits * 2.5 == .49 m/s
        '''
        robot_dict[sphero_name].call_velocity(vel_topic_list[i]) #subscriber. User can publish to /sphero/vel# in Twist message type to change speed
       
        ##DONT EDIT BELOW THIS LINE. NOT WORKING##
        #TODO: IMU/odom
        '''
        Start up odometry and Imu data
        '''
        #robot_dict[sphero_name].call_odom_imu(odom_topic_list[i], imu_topic_list[i]) #subscriber. User can publish to /sphero/vel# in Twist message type to change speed


if __name__ == '__main__':
    on = begin(name_list)
    topic_start(color_topic_list,vel_topic_list,odom_topic_list,imu_topic_list)
    rospy.spin()


