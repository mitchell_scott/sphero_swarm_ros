# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 10:49:37 2017

@author: exalabs4
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 10:47:33 2017

@author: exalabs4
"""

#!/usr/bin/env python
import rospy
from std_msgs.msg import Bool
import math
import sys
import time

'''
import dynamic_reconfigure.server
from sphero_driver import sphero_driver2 as sphero_driver
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig
from sphero_node.msg import VelGroup
import yaml
import os.path
import subprocess
'''
class robot_move():

    def __init__(self):
        self.robot_number = 8
        self.x_data = 0
        self.y_data = 0
        self.z = 0
        self.position = []
        self.time_inital = time.time()
        self.heading = []
        self.speed_list = []
        self.frequency = math.pi/3 #8*math.pi
        #print(self.frequency)
        self.robot_speed = 40 #bits
        self.current_time = 0.0
        #self.pub = rospy.Publisher('xy_values', VelGroup, queue_size = 10)


    def move(self):
        r = rospy.Rate(12)
        while not rospy.is_shutdown(): 
            stab1 = '/disable_stabilization1'
            stab2 = '/disable_stabilization2'
            stab3 = '/disable_stabilization3'
            stab4 = '/disable_stabilization4'
            stab5 = '/disable_stabilization5'
            stab6 = '/disable_stabilization6'
            stab7 = '/disable_stabilization7'
            pub1 = rospy.Publisher(stab1, Bool, queue_size = 10)
            pub2 = rospy.Publisher(stab2, Bool, queue_size = 10)
            pub3 = rospy.Publisher(stab3, Bool, queue_size = 10)
            pub4 = rospy.Publisher(stab4, Bool, queue_size = 10)
            pub5 = rospy.Publisher(stab5, Bool, queue_size = 10)
            pub6 = rospy.Publisher(stab6, Bool, queue_size = 10)
            pub7 = rospy.Publisher(stab7, Bool, queue_size = 10)
            cmd = Bool()
            cmd.data = True
            pub1.publish(cmd)
            pub2.publish(cmd)
            pub3.publish(cmd)
            pub4.publish(cmd)
            pub5.publish(cmd)
            pub6.publish(cmd)
            pub7.publish(cmd)
            #print(self.heading)
            r.sleep()

    
    def stop(self):
        for i in range(0,self.robot_number):
            current_time = self.time_inital - time.time()
            move = Twist()
            x_vel = 0
            y_vel = 0
            self.x_vel.append(x_vel)
            self.y_vel.append(y_vel)

            move.linear.x = self.x_vel
            move.linear.y = self.y_vel
            self.pub.publish(move)


    def normalize_angle_positive(self, angle):
        return math.fmod(math.fmod(angle, 2.0*math.pi) + 2.0*math.pi, 2.0*math.pi);
 


if __name__ == '__main__':
    rospy.init_node('move', anonymous=True)
    time.sleep(0.5)
    r = robot_move()
    try:
        r.move()
    except KeyboardInterrupt:
        stop = r.stop()
        if stop:
            sys.exit(1)
        else:
            stop = r.stop()
            sys.exit(1)
    