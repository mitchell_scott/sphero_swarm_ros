# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 10:47:33 2017

@author: exalabs4
"""

#!/usr/bin/env python
import rospy
from std_msgs.msg import ColorRGBA, Float32
import math
import sys
import time

class robot_move():

    def __init__(self):
        self.robot_number = 8
        self.x_data = 0
        self.y_data = 0
        self.z = 0
        self.position = []
        self.time_inital = time.time()
        self.heading = []
        self.speed_list = []
        self.frequency = math.pi/3 #8*math.pi
        #print(self.frequency)
        self.robot_speed = 40 #bits
        self.current_time = 0.0

    def move(self):
        r = rospy.Rate(12)
        while not rospy.is_shutdown(): 
            '''
            Main light off
            '''
            col = ColorRGBA()
            self.heading = []
            self.speed_list = []
            self.current_time = time.time() - self.time_inital
            for i in range(0, 50):
                col_topic = '/set_color%s' % (i)
                pub1 = rospy.Publisher(col_topic, ColorRGBA, queue_size = 10)
                col.r = 0
                col.g = 0
                col.b = 0
                col.a = 0
                pub1.publish(col)
                #print(self.heading)
            '''
            Back Light Off
            
            back = Float32()
            self.heading = []
            self.speed_list = []
            self.current_time = time.time() - self.time_inital
            for i in range(0, 50):
                back_topic1 = '/set_back_led%s' % (i)
                pub1 = rospy.Publisher(back_topic1, Float32, queue_size = 10)
                back.data = 0.0
                pub1.publish(back)
            r.sleep()
            '''
    
    def stop(self):
        for i in range(0,self.robot_number):
            current_time = self.time_inital - time.time()
            move = Twist()
            x_vel = 0
            y_vel = 0
            self.x_vel.append(x_vel)
            self.y_vel.append(y_vel)

            move.linear.x = self.x_vel
            move.linear.y = self.y_vel
            self.pub.publish(move)


    def normalize_angle_positive(self, angle):
        return math.fmod(math.fmod(angle, 2.0*math.pi) + 2.0*math.pi, 2.0*math.pi);
 


if __name__ == '__main__':
    rospy.init_node('color_off')
    time.sleep(1.0)
    r = robot_move()
    try:
        r.move()
    except KeyboardInterrupt:
        stop = r.stop()
        if stop:
            sys.exit(1)
        else:
            stop = r.stop()
            sys.exit(1)
    