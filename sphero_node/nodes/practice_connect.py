#!/usr/bin/env python

#***********************************************************
#* Software License Agreement (BSD License)
#*
#*  Copyright (c) 2012, Melonee Wise
#*  All rights reserved.
#*
#*  Redistribution and use in source and binary forms, with or without
#*  modification, are permitted provided that the following conditions
#*  are met:
#*
#*   * Redistributions of source code must retain the above copyright
#*     notice, this list of conditions and the following disclaimer.
#*   * Redistributions in binary form must reproduce the above
#*     copyright notice, this list of conditions and the following
#*     disclaimer in the documentation and/or other materials provided
#*     with the distribution.
#*
#*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#*  POSSIBILITY OF SUCH DAMAGE.
#***********************************************************
#author: Mitchell Scott, with useage of sphero_ros work by Melonee Wise

import sys
import rospy
import math
import sys
import tf
import PyKDL
import bluetooth



from sphero_driver import sphero_driver2 as sphero_driver
import dynamic_reconfigure.server

from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig

from sphero_class import sphero_start

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'


class start:
    def __init__(self):
        #Inital Information
        self.is_connected = False
        #Find nearby sphero 
        self.target_name = 'Sphero' #this allows for bluetooth to lookup local spheros
        self.name_list = []
        self.address_list = []
        self.color_topic_list = []
        self.vel_topic_list = []
        self.odom_topic_list = []
        self.imu_topic_list = []
        self.robot_list = []
        self.port_list = []
        self.sphero_dict = {} #to start the spheros/send data via. bluetooth
        self.robot_dict = {} #to subscribe/publish topics
        self.nearby_devices = bluetooth.discover_devices(lookup_names = True)
        #print(self.nearby_devices)
        if len(self.nearby_devices)>0:
            for bdaddr, name in self.nearby_devices:
                #look for a device name that starts with Sphero
                if name.startswith(self.target_name):
                    self.name_list.append(name)
                    self.address_list.append(bdaddr)
                    if len(self.name_list) <= 7:
                        self.port_list.append(1)
                    elif len(self.name_list):
                        self.port_list.append(2)
            print('Devices to connect to:      ')
            print('name_list', self.name_list)
            print('address', self.address_list)
            print('I found %s nearby devices') % (len(self.nearby_devices)) 
            for i in range(0,len(self.name_list)):
                color_topic = 'sphero/color%s' % (i+1)
                vel_topic = 'sphero/velocity%s' % (i+1)
                odom_topic = 'sphero/odom%s' % (i+1)
                imu_topic = 'sphero/imu%s' % (i+1)
                self.color_topic_list.append(color_topic)
                self.vel_topic_list.append(vel_topic)
                self.odom_topic_list.append(odom_topic)
                self.imu_topic_list.append(imu_topic)
        #print('inital vel topic', self.vel_topic_list)
        self.connected_list = [False]*len(self.nearby_devices) #List which tell the actual robots which connected
    def begin(self):
        for i in range(0,len(self.name_list)):
            sphero_name = 'sphero%s' % (i+1)
            self.sphero_dict[sphero_name] = sphero_driver.Sphero(self.name_list[i], self.address_list[i], self.port_list[i])

    def initalize(self):
        for i in range(0,len(self.name_list)):
            sphero_name = 'sphero%s' % (i+1)
            self.robot_list.append(sphero_name)
            self.robot_dict[sphero_name] = sphero_start() #With the numbr of spheros defined by the sphero class, we can now initalzie topics for each of the spheros
            initalize = self.robot_dict[sphero_name].initalize(self.sphero_dict[sphero_name]) #Must run for the spheros to actually connect via. bluetooth
            #print('initalize',initalize)
            if initalize:
                #print('in inital')
                self.connected_list[i] = True #if it connected, set to true
                print('%s is connected') % (sphero_name)
                #self.robot_dict[sphero_name].call_color(self.color_topic_list[i]) #subscriber. User can publish to /sphero/color# in RGBA message type to change colors
                '''
                NOTE: call_velocity publication is in bits/255 with a max speed of 2.5 m/s. So a command of 50 is equal to 50/255 bits * 2.5 == .49 m/s
                '''
                #self.robot_dict[sphero_name].call_velocity(self.vel_topic_list[i]) #subscriber. User can publish to /sphero/vel# in Twist message type to change speed
       
                ##DONT EDIT BELOW THIS LINE. NOT WORKING##
                #TODO: IMU/odom
                '''
                Start up odometry and Imu data
                '''
                #robot_dict[sphero_name].call_odom_imu(odom_topic_list[i], imu_topic_list[i]) #subscriber. User can publish to /sphero/vel# in Twist message type to change speed


        #topic = self.topic_start(self.connected_list)
        #if topic:
        #    return True
    def topic_start(self,connected_list):
        self.connected_list = connected_list
        #print('connected_list', self.connected_list)
        #print('topic vel list', self.vel_topic_list)
        #print('robot dict',self.robot_dict)
        color_topic_list_reduced = [j for i,j in enumerate(self.color_topic_list) if self.connected_list[i] == True]
        vel_topic_list_reduced = [j for i,j in enumerate(self.vel_topic_list) if self.connected_list[i] == True]
        new_robot_list = [j for i,j in enumerate(self.robot_list) if self.connected_list[i] == True]
        print(new_robot_list)
        for i in range(0,len(color_topic_list_reduced)): 
            sphero_name = new_robot_list[i] 
            #print('color_topic',color_topic_list_reduced[i])
            #print('vel_topic',vel_topic_list_reduced[i])
            #try:
            color_topic = 'sphero/color%s' % (i+1)
            vel_topic = 'sphero/vel%s' % (i+1)
            call_color = self.robot_dict[sphero_name].call_color(color_topic)
            call_velocity = self.robot_dict[sphero_name].call_velocity(vel_topic)
            #print(color.Green + 'Everything is great! Go Cougs! You may move the spheros now.' + color.END)
            #except:
            #print(color.RED + 'Failure. Exciting. Check bluettoth connection, try again.' + color.END)
            #sys.exit(1)
            #if call_color and call_velocity:
                #print('Good')
            #elif call_color and not call_velocity:
                #print('vel failed')
            #elif call_velocity and not call_color:
                #print('color failed')
            #else:
                #print('both failed')
        return True

                    

if __name__ == '__main__':
    start = start()
    on = start.begin()
    start.initalize()
    #topic_start(color_topic_list,vel_topic_list,odom_topic_list,imu_topic_list)













