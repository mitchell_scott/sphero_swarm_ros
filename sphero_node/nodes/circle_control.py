#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped, Pose, Twist
from sensor_msgs.msg import Imu
import numpy as np
import math
import sys
import time


import dynamic_reconfigure.server
from sphero_driver import sphero_driver2 as sphero_driver
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig
from sphero_node.msg import VelGroup
import yaml
import os.path
import subprocess

class robot_move():

    def __init__(self):
        #path = os.path.abspath('move_params.yaml')
        filepath = subprocess.check_output(["rospack", "find", "sphero_node"])
        filepath_str = filepath.strip()
        with open(filepath_str.decode("UTF-8") + '/params/move_params.yaml', 'r') as stream:
            parameters = yaml.load(stream)
        self.robot_number = parameters['data']['robot_number_inital']
        self.x_data = 0
        self.y_data = 0
        self.z = 0
        self.position = []
        self.time_inital = time.time()
        self.heading = []
        self.speed_list = []
        self.frequency = math.pi #8*math.pi
        self.frequency = parameters['data']['frequency']*math.pi*2
        #print(self.frequency)
        self.robot_speed = 40 #bits
        self.pub = rospy.Publisher('xy_values', VelGroup, queue_size = 10)

    def move(self):
        r = rospy.Rate(50)
        while not rospy.is_shutdown(): 
            vel_group = VelGroup()
            self.heading = []
            self.speed_list = []
            for i in range(0,self.robot_number):
                current_time = self.time_inital - time.time()
                x_vel = self.robot_speed*math.cos(self.frequency*time.time())
                y_vel = self.robot_speed*math.sin(self.frequency*time.time())
                real_angle = math.atan2(y_vel,x_vel)
                if abs(x_vel) == 0:
                    x_vel = 0
                if abs(y_vel) == 0:
                    y_vel = 0
                heading_ang = math.degrees(self.normalize_angle_positive(math.atan2(y_vel,x_vel)))
                self.heading.append(float(heading_ang)) 
                self.speed_list.append(self.robot_speed)
            vel_group.heading = self.heading
            vel_group.speed = self.speed_list
            self.pub.publish(vel_group) 
            #print(self.heading)
            r.sleep()

    
    def stop(self):
        for i in range(0,self.robot_number):
            current_time = self.time_inital - time.time()
            x_vel = 0
            y_vel = 0
            self.x_vel.append(x_vel)
            self.y_vel.append(y_vel)

            vel_group.x_vel = self.x_vel
            vel_group.y_vel = self.y_vel
            self.pub.publish(vel_group)


    def normalize_angle_positive(self, angle):
        return math.fmod(math.fmod(angle, 2.0*math.pi) + 2.0*math.pi, 2.0*math.pi);
 


if __name__ == '__main__':
    rospy.init_node('move', anonymous=True)
    r = robot_move()

    try:
        r.move()
    except KeyboardInterrupt:
        stop = r.stop()
        if stop:
            sys.exit(1)
        else:
            stop = r.stop()
            sys.exit(1)
    
