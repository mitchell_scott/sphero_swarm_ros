#!/usr/bin/env python

#***********************************************************
#* Software License Agreement (BSD License)
#*
#*  Copyright (c) 2012, Melonee Wise
#*  All rights reserved.
#*
#*  Redistribution and use in source and binary forms, with or without
#*  modification, are permitted provided that the following conditions
#*  are met:
#*
#*   * Redistributions of source code must retain the above copyright
#*     notice, this list of conditions and the following disclaimer.
#*   * Redistributions in binary form must reproduce the above
#*     copyright notice, this list of conditions and the following
#*     disclaimer in the documentation and/or other materials provided
#*     with the distribution.
#*
#*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#*  POSSIBILITY OF SUCH DAMAGE.
#***********************************************************
#author: Mitchell Scott, with useage of sphero_ros work by Melonee Wise
import sys
import pdb
import sys
import rospy
import math
import sys
import tf
import PyKDL
import bluetooth
import time
from multi_streaming import many_streaming
from multiprocessing import Process


from sphero_driver import sphero_driver3 as sphero_driver
import dynamic_reconfigure.server

from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig

from sphero_class import sphero_start
#from odom_imu import *



class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'


class connect:
    def __init__(self):
        self.is_connected = False
        self.time_inital = rospy.get_time()
        self.target_name = 'Sphero' #this allows for bluetooth to lookup local spheros
        self.name_list = []
        self.address_list = []
        self.color_topic_list = []
        self.vel_topic_list = []
        self.odom_topic_list = []
        self.imu_topic_list = []
        self.robot_list = []
        self.port_list = []
        self.streaming_list = [] # check which robots are actually streaming
        self.sphero_dict = {} #to start the spheros/send data via. bluetooth
        self.robot_dict = {} #to subscribe/publish topics
        self.connected_dictionary = {}
        self.tryagain_sphero_dictionary = {}
        self.tryagain_robot_dict = {}
        self.nearby_devices = bluetooth.discover_devices(duration=20, lookup_names = True) # see which bluetooth devices are nearby 
        print(self.nearby_devices)
        self.call_color = False
        self.call_velocity = False
        self.call_imu = call_imu_odom = False
        self.connected_robots_list = []
        self.failed_robots_list = []
        self.port_conenct_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
        i = 0
        if len(self.nearby_devices)>0:
            for bdaddr, name in self.nearby_devices:
                if name.startswith(self.target_name):
                    '''
                    Append names and address of all the bluetooth devices in the area that start with the bame 'Sphero'.
                    '''
                    if name == 'Sphero-AAA':
                      pass
                    else:
                      #print('no pass')
                      self.name_list.append(name)
                      self.address_list.append(bdaddr)
                      if len(self.name_list) <= 7:
                        self.port_list.append(1)  #Tell which bluetooth port to connect to
                      elif len(self.name_list) > 7:
                       self.port_list.append(1)
                i+=1
            print('I found %s robots to connect to') % (len(self.name_list))
            print('Devices to connect to:      ')
            print('name_list', self.name_list)
            print('address', self.address_list)
            for i in range(0,len(self.name_list)):
                color_topic = 'sphero/color%s' % (i+1)
                vel_topic = 'sphero/velocity%s' % (i+1)
                odom_topic = 'sphero/odom%s' % (i+1)
                imu_topic = 'sphero/imu%s' % (i+1)
                self.color_topic_list.append(color_topic)
                self.vel_topic_list.append(vel_topic)
                self.odom_topic_list.append(odom_topic)
                self.imu_topic_list.append(imu_topic)
        self.connected_list = [False]*len(self.name_list) #List which tell the actual robots which connected

class start:
    def __init__(self,name,address,port,number):
        self.name = name
        self.address = address
        self.port = port
        self.number = number
        self.maximum_connect_attemps = 4 #Number of times the sphero will attempt to conenct before failure 
        self.connected = False
    def begin(self):
        '''
        Tell the sphero drive the name address and port to connect to
        '''
        self.sphero = sphero_driver.Sphero(self.number, self.time_inital, self.name, self.address, self.port)
        #print('sphero1', self.sphero)
        connect.robot_dict[self.number] = self.sphero
    def initalize_start(self):
        '''
        Try to connect the spheros. If successful, it will initalize the subscribers and record the connected robots to a dictionary 
        '''
        self.robot = sphero_start()
        #print('sphero2', self.sphero)
        initalize= self.robot.initalize(self.number, self.sphero) #Must run for the spheros to actually connect via. bluetooth
        trial_number = 0
        if initalize:
           self.connected = True #if it connected, set to true
        else:
            while not self.connected:
                trial_number += 1
                if trial_number > self.maximum_connect_attemps:
                    break
                self.robot = sphero_start()
                initalize= self.robot.initalize(self.number, self.sphero)
                
    """
    def data_run(self):
        '''
        Start reciving bluetooth data and publish imu and odom messages for ROS. Tells which robots are connected and which to stream data from
        '''
        if self.connected == True:
            begin_streaming = many_streaming()
            organize.organize(self.connected_dictionary)  
    """

    def odom_imu(self):
        '''
        Takes in the list of connected robots. If the robots are connected, initalize the odom and imu publishers
        
        odom_topic = 'sphero/odom%s' % (number)
        imu_topic = 'sphero/imu%s' % (number)
        diag_topic = 'sphero/diag%s' % (number) 
        ''' 
        self.call_imu = self.robot.call_odom_imu()
        return True   

def run(name_list):
    robot_list = []
    sphero = {}
    for i in range(0,len(name_list)):
        #print(i)
        name = name_list[i]
        address = address_list[i]
        port = port_list[i]
        number = i+1
        #robot = 
        start_spheros = main(name,address,port,number)
        sphero[number] = start_spheros
        #p = Process(target = main, args=(name,address,port,number))
        #print(p)
        #p.start()
        #processes.append(p)
        #robot_list.append(robot)
    for i in range(0,len(name_list)):
        name = name_list[i]
        number = i+1
        #streaming(number, connect.robot_dict[i+1])
        p = Process(target = streaming, args=(number, connect.robot_dict[i+1], sphero[i+1]))
        p.start()
        processes.append(p)
        print('iteration', i )
    for i in range(0,len(name_list)):
        processes[i].join() #What does this do? 


    print(color.GREEN + 'Everything is great! Go Cougs! You may move the spheros now.' + color.END)
    rospy.spin()
    #return True

def main(name,address,port,number):
    start_spheros = start(name,address,port,number) #connect to the nodes and begin streaming data     
    #print("Connecting ... be patient")
    print(name)
    start_spheros.begin()
    inital_start = start_spheros.initalize_start() 
    #odom = start_spheros.odom_imu()
    #bt_stream = many_streaming()
    #robot = connect.robot_dict[number]
    #print('robot', name)
    #print(connect.robot_dict)
    #bt_stream.stream(name, robot)
    return start_spheros

def streaming(number, robot, sphero):
    #inital_start = sphero.initalize_start() 
    odom = sphero.odom_imu()
    bt_stream = many_streaming()
    bt_stream.stream(number, robot)


if __name__ == '__main__':
    print("Searching for spheros...")
    processes = []
    rospy.init_node('sphero')
    connect = connect() #determine the name, address, and port to connect to
    name_list = connect.name_list
    address_list = connect.address_list
    port_list = connect.port_list
    #while not rospy.is_shutdown():
    run(connect.name_list)
    #for i in range(0,len(processes)):
        #processes[i].join()
    #if run:
        #print(color.GREEN + 'Everything is great! Go Cougs! You may move the spheros now.' + color.END)
        #time.sleep(2)
        #rospy.spin()
    #else:
    #print(color.RED + 'Failure. Exiting. Check bluettoth connection, try again.' + color.END)
    #sys.exit(1)













"""
    def data_run(self):
        '''
        Start reciving bluetooth data and publish imu and odom messages for ROS. Tells which robots are connected and which to stream data from
        '''
        for i in range(0,len(self.new_robot_list)):
            sphero_name = 'sphero%s' % (i+1)
            if self.connected_list[i] == True:
                self.connected_dictionary[sphero_name] = self.robot_dict[sphero_name]
        key_list = self.connected_dictionary.keys()
        odom_imu = imu_start()
        odom_imu.run_rec(self.connected_dictionary) 
        #odom_imu.recieve(len(key_list))    
"""










