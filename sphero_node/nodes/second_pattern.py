# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 10:47:33 2017

@author: exalabs4
"""

#!/usr/bin/env python
import rospy
from std_msgs.msg import String, ColorRGBA
from geometry_msgs.msg import PoseStamped, Pose, Twist
from sensor_msgs.msg import Imu
import numpy as np
import math
import sys
import time

'''
import dynamic_reconfigure.server
from sphero_driver import sphero_driver2 as sphero_driver
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig
from sphero_node.msg import VelGroup
import yaml
import os.path
import subprocess
'''
class robot_move():

    def __init__(self):
        self.robot_number = 8
        self.x_data = 0
        self.y_data = 0
        self.z = 0
        self.position = []
        self.time_inital = time.time()
        self.heading = []
        self.speed_list = []
        self.frequency = math.pi/2 #8*math.pi
        #print(self.frequency)
        self.robot_speed = 60 #bits
        self.current_time = 0.0
        #self.pub = rospy.Publisher('xy_values', VelGroup, queue_size = 10)
    '''
    def move(self):
        r = rospy.Rate(50)
        while not rospy.is_shutdown(): 
            move = Twist()
            self.heading = []
            self.speed_list = []
            for i in range(0,self.robot_number):
                current_time = self.time_inital - time.time()
                x_vel = self.robot_speed*math.cos(self.frequency*time.time())
                y_vel = self.robot_speed*math.sin(self.frequency*time.time())
                vel_topic = '/cmd_vel%s' % (i+1)
                pub = rospy.Publisher(vel_topic, Twist, queue_size = 10)
                real_angle = math.atan2(y_vel,x_vel)
                if abs(x_vel) == 0:
                    x_vel = 0
                if abs(y_vel) == 0:
                    y_vel = 0
                print(vel_topic, i)
                move.linear.x = x_vel
                move.linear.y = y_vel
                pub.publish(move)
                
            #print(self.heading)
            r.sleep()
    '''

    def move(self):
        r = rospy.Rate(12)
        while not rospy.is_shutdown(): 
            '''
            NODE 1
            '''
            move = Twist()
            self.heading = []
            self.speed_list = []
            self.current_time = time.time() - self.time_inital
            col = ColorRGBA()
            #x_vel = self.robot_speed*math.cos(self.frequency*time.time())
            #y_vel = self.robot_speed*math.sin(self.frequency*time.time())
            print(self.current_time)
            if self.current_time < 3:
                col.r = 1.0
                x_vel = 30
                y_vel = 0
            if self.current_time > 3 and self.current_time <6:
                col.r = 0.0
                col.b = 1.0
                x_vel = -30
                y_vel = 0
            if self.current_time > 6:
                col.r = 0.0
                col.b = 0.0
                col.g = 1.0
                x_vel = self.robot_speed*math.cos(self.frequency*time.time())
                y_vel = self.robot_speed*math.sin(self.frequency*time.time())
            for i in range(0, 50):
                vel_topic1 = '/cmd_vel%s' % (i)
                pub1 = rospy.Publisher(vel_topic1, Twist, queue_size = 10)
                if abs(x_vel) == 0:
                    x_vel = 0
                if abs(y_vel) == 0:
                    y_vel = 0
                print('x', x_vel)
                print('y', y_vel)
                move.linear.x = x_vel
                move.linear.y = y_vel
                pub1.publish(move)
            for i in range(0, 10):
                col_topic = '/set_color%s' % (i + 1)
                col_pub = rospy.Publisher(col_topic, ColorRGBA, queue_size = 10)
                col_pub.publish(col)
                #print(self.heading)
            '''
            NODE 2
            
            move = Twist()
            self.heading = []
            self.speed_list = []
            self.current_time = time.time() - self.time_inital
            #x_vel = self.robot_speed*math.cos(self.frequency*time.time())
            #y_vel = self.robot_speed*math.sin(self.frequency*time.time())
            print(self.current_time)
            if self.current_time < 4:
                x_vel = 20
                y_vel = 0
            if self.current_time > 4 and self.current_time <7:
                x_vel = 20
                y_vel = 0
            if self.current_time > 7:
                x_vel = self.robot_speed*math.cos(self.frequency*time.time())
                y_vel = self.robot_speed*math.sin(self.frequency*time.time())
            for i in range(10, 17):
                vel_topic1 = '/cmd_vel%s' % (i)
                pub1 = rospy.Publisher(vel_topic1, Twist, queue_size = 10)
                if abs(x_vel) == 0:
                    x_vel = 0
                if abs(y_vel) == 0:
                    y_vel = 0
                print('x', x_vel)
                print('y', y_vel)
                move.linear.x = x_vel
                move.linear.y = y_vel
                pub1.publish(move)
                #print(self.heading)
                '''
            r.sleep()

    
    def stop(self):
        for i in range(0,self.robot_number):
            current_time = self.time_inital - time.time()
            move = Twist()
            x_vel = 0
            y_vel = 0
            self.x_vel.append(x_vel)
            self.y_vel.append(y_vel)

            move.linear.x = self.x_vel
            move.linear.y = self.y_vel
            self.pub.publish(move)


    def normalize_angle_positive(self, angle):
        return math.fmod(math.fmod(angle, 2.0*math.pi) + 2.0*math.pi, 2.0*math.pi);
 


if __name__ == '__main__':
    rospy.init_node('move', anonymous=True)
    time.sleep(1.0)
    r = robot_move()
    try:
        r.move()
    except KeyboardInterrupt:
        stop = r.stop()
        if stop:
            sys.exit(1)
        else:
            stop = r.stop()
            sys.exit(1)
    