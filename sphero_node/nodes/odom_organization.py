import rospy 
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose
from sphero_node.msg import OdometryList
from params import *
import sys


class Odom():

    def __init__(self):
        '''
        Take in Odometry data and organize it into one lsit
        '''
        self.x = []
        self.y = []
        self.yaw = []
        self.o = odom_p()

    def main(self,odom_topic,sphero_name,x_dict,y_dict,yaw_dict):
        self.sphero_name = sphero_name
        self.x_dict = x_dict
        self.y_dict = y_dict
        self.yaw_dict = yaw_dict
        rospy.Subscriber(odom_topic, Odometry, self.callback) 

    def callback(self,msg):
        self.x_pos = msg.pose.pose.position.x
        self.y_pos = msg.pose.pose.position.y
        self.yaw = msg.pose.pose.orientation.z     
        self.move()
    def move(self):
        self.o.odom_list(self.x_pos,self.y_pos,self.yaw,self.sphero_name,self.x_dict,self.y_dict,self.yaw_dict)

class odom_p:
    def __init__(self):
        self.cat = {}

    def odom_list(self,x_pos,y_pos,yaw,sphero_name,x_dict,y_dict,yaw_dict):
        self.x_dict = x_dict
        self.y_dict = y_dict
        self.yaw_dict = yaw_dict
        self.x_dict[sphero_name] = x_pos
        self.y_dict[sphero_name] = y_pos
        self.yaw_dict[sphero_name] = yaw    
        self.odom_publish()

    def odom_publish(self):
        key_list = self.x_dict.keys() 
        x = []
        y = [] 
        yaw = []
        pub = rospy.Publisher('odom_list', OdometryList, queue_size = 10)
        for i in range(0,robot_number_inital):
            odom_list = OdometryList()
            sphero_name = 'sphero%s' % (i+1)
            x.append(self.x_dict[sphero_name])
            y.append(self.y_dict[sphero_name])
            yaw.append(self.yaw_dict[sphero_name])
        odom_list.x = x
        odom_list.y = y
        odom_list.yaw = yaw
        pub.publish(odom_list)    

def start(args):
    '''Initializes and cleanup ros node'''
    odom = Odom()
    x_dict = {}
    y_dict = {}
    yaw_dict = {}

    for j in range(0,robot_number_inital):
        sphero_name = 'sphero%s' % (j)
        x_dict[sphero_name] = []
        y_dict[sphero_name] = []
        yaw_dict[sphero_name] = []
    i = 1
    while not rospy.is_shutdown():
        if i <= robot_number_inital:
            number = i 
        elif i > robot_number_inital:
            number = i % robot_number_inital 
            if number == 0:
                number = robot_number_inital
        i = i + 1
        sphero_name = 'sphero%s' % (number)
        odom_topic = '/odom%s' % (number)
        odom.main(odom_topic,sphero_name,x_dict,y_dict,yaw_dict)


if __name__ == '__main__':
    rospy.init_node('odom_estimation', anonymous=True)
    start(sys.argv)
    sys.exit(1)

