#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped, Pose, Twist
from sensor_msgs.msg import Imu
import numpy as np
import math
import sys
import time
from control.matlab import *
'''
import dynamic_reconfigure.server
from sphero_driver import sphero_driver2 as sphero_driver
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig
from sphero_node.msg import VelGroup
import yaml
import os.path
import subprocess
'''
class robot_move():

    def __init__(self):
        self.robot_number = 8
        self.x_data = 0
        self.y_data = 0
        self.z = 0
        self.position = []
        self.time_inital = time.time()
        self.heading = []
        self.speed_list = []
        self.frequency = math.pi/3 #8*math.pi
        #print(self.frequency)
        self.robot_speed = 40 #bits
        self.current_time = 0.0
        #self.pub = rospy.Publisher('xy_values', VelGroup, queue_size = 10)
    '''
    def move(self):
        r = rospy.Rate(50)
        while not rospy.is_shutdown(): 
            move = Twist()
            self.heading = []
            self.speed_list = []
            for i in range(0,self.robot_number):
                current_time = self.time_inital - time.time()
                x_vel = self.robot_speed*math.cos(self.frequency*time.time())
                y_vel = self.robot_speed*math.sin(self.frequency*time.time())
                vel_topic = '/cmd_vel%s' % (i+1)
                pub = rospy.Publisher(vel_topic, Twist, queue_size = 10)
                real_angle = math.atan2(y_vel,x_vel)
                if abs(x_vel) == 0:
                    x_vel = 0
                if abs(y_vel) == 0:
                    y_vel = 0
                print(vel_topic, i)
                move.linear.x = x_vel
                move.linear.y = y_vel
                pub.publish(move)
                
            #print(self.heading)
            r.sleep()
    '''

    def move(self):
        r = rospy.Rate(12)
        while not rospy.is_shutdown(): 
            move = Twist()
            self.heading = []
            self.speed_list = []
            self.current_time = time.time() - self.time_inital
            #x_vel = self.robot_speed*math.cos(self.frequency*time.time())
            #y_vel = self.robot_speed*math.sin(self.frequency*time.time())
            print(self.current_time)
            if self.current_time < 3:
                x_vel = 30
                y_vel = 0
            if self.current_time > 3 and self.current_time <6:
                x_vel = -30
                y_vel = 0
            if self.current_time > 6:
                x_vel = self.robot_speed*math.cos(self.frequency*time.time())
                y_vel = self.robot_speed*math.sin(self.frequency*time.time())
            vel_topic1 = '/cmd_vel1'
            vel_topic2 = '/cmd_vel2'
            vel_topic3 = '/cmd_vel4'
            vel_topic4 = '/cmd_vel6'
            vel_topic5 = '/cmd_vel5'
            vel_topic6 = '/cmd_vel7'
            vel_topic7 = '/cmd_vel9'
            pub1 = rospy.Publisher(vel_topic1, Twist, queue_size = 10)
            pub2 = rospy.Publisher(vel_topic2, Twist, queue_size = 10)
            pub3 = rospy.Publisher(vel_topic3, Twist, queue_size = 10)
            pub4 = rospy.Publisher(vel_topic4, Twist, queue_size = 10)
            pub5 = rospy.Publisher(vel_topic5, Twist, queue_size = 10)
            pub6 = rospy.Publisher(vel_topic6, Twist, queue_size = 10)
            pub7 = rospy.Publisher(vel_topic7, Twist, queue_size = 10)
            real_angle = math.atan2(y_vel,x_vel)
            if abs(x_vel) == 0:
                x_vel = 0
            if abs(y_vel) == 0:
                y_vel = 0
            print('x', x_vel)
            print('y', y_vel)
            move.linear.x = x_vel
            move.linear.y = y_vel
            pub1.publish(move)
            pub2.publish(move)
            pub3.publish(move)
            pub4.publish(move)
            pub5.publish(move)
            pub6.publish(move)
            #pub7.publish(move)
            #print(self.heading)
            r.sleep()

    
    def stop(self):
        for i in range(0,self.robot_number):
            current_time = self.time_inital - time.time()
            move = Twist()
            x_vel = 0
            y_vel = 0
            self.x_vel.append(x_vel)
            self.y_vel.append(y_vel)

            move.linear.x = self.x_vel
            move.linear.y = self.y_vel
            self.pub.publish(move)


    def normalize_angle_positive(self, angle):
        return math.fmod(math.fmod(angle, 2.0*math.pi) + 2.0*math.pi, 2.0*math.pi);
 


if __name__ == '__main__':
    rospy.init_node('move', anonymous=True)
    time.sleep(0.5)
    r = robot_move()
    try:
        r.move()
    except KeyboardInterrupt:
        stop = r.stop()
        if stop:
            sys.exit(1)
        else:
            stop = r.stop()
            sys.exit(1)
    