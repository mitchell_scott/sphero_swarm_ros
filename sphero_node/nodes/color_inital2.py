from params import *
import rospy
import time
from std_msgs.msg import ColorRGBA, Bool, Float32MultiArray
import sys
import json

class change_color:
    def __init__(self,number,x_dict,y_dict):
        self.robot_number = robot_number_inital
        self.color_callback = rospy.Subscriber('/ObjectData', Float32MultiArray, self.callback)
        self.number = number
        self.i = 0
        self.x_dict = x_dict
        self.y_dict = y_dict
        #if number >= self.robot_number+1:
        #    sys.exit(1)


 
    def color_change(self):
        i = 1
        color = ColorRGBA()
        #while not rospy.is_shutdown():
        if i <= self.robot_number:
            number = i 
        elif i > self.robot_number:
            number = i % self.robot_number 
            if number == 0:
                number = self.robot_number
        i = i + 1
        topic_name = '/set_color%s' % (number)
        color.r = 255
        pub = rospy.Publisher(topic_name,ColorRGBA,queue_size = 10)
        print(topic_name)
        pub.publish(color)
        #print(data)
        #if i > self.robot_number:
        #    time.sleep(5)
        #color.r = 0
        #pub = rospy.Publisher(topic_name,ColorRGBA,queue_size = 10)
        #pub.publish(color)
        #if i > self.robot_number:
        #    time.sleep(5)
        #if i > 2*self.robot_number:
        #    sys.exit(1)


    def callback(self,data):
        if self.number <= self.robot_number:
            number = self.number
        else:
            number = self.number - self.robot_number
        topic_name = '/set_color%s' % (number)
        #print(data)
        color = ColorRGBA()
        color.r = 255
        pub = rospy.Publisher(topic_name,ColorRGBA,queue_size = 10)
        #print(self.number)
        pub.publish(color)
        if self.number > self.robot_number:
            time.sleep(2)
            #print(data)
            x_value = data.data[0]
            y_value = data.data[1]
            print(x_value)
            #x_phrase = '%spheros_x = %s\n' % (number, x_value) 
            #y_phrase = '%s_y = %s\n' % (number, y_value)
            #f.write(x_phrase)
            #f.write(y_phrase)
            sphero_name = 'sphero%s' % (number)
            self.x_dict[sphero_name] = x_value
            self.y_dict[sphero_name] = y_value
        
        color.r = 0
        pub = rospy.Publisher(topic_name,ColorRGBA,queue_size = 10)
        pub.publish(color)
        if self.number > self.robot_number:
            time.sleep(2)
         
        rospy.sleep(.2)
        #print(self.x_dict)
        self.list_dict(self.x_dict,self.y_dict)

    def list_dict(self,x_dict,y_dict):
        key_len = len(x_dict.keys())
        #for i in range(0, key_len):
        #    sphero_name = 'sphero%s' % (i+1)
        with open('x_ip.json', 'w') as f1:
            json.dump(x_dict,f1)
        with open('y_ip.json', 'w') as f2:
            json.dump(y_dict,f2)
        self.color_callback.unregister() 



"""

def callback(data):

    for i in range(0, robot_number_inital):
        topic_name = '/set_color%s' % (i+1)
        color = ColorRGBA()
        color.r = 255
        pub = rospy.Publisher(topic_name,ColorRGBA,queue_size = 10)
        pub.publish(color)
        print(data)
        #time.sleep(2)
        color.r = 0
        pub = rospy.Publisher(topic_name,ColorRGBA,queue_size = 10)
        #time.sleep(1)
        robot_number = data.data[2]
        x_value = data.data[1]
        y_value = data.data[1]
        rospy.sleep(.0001) 
    #self.color_callback.unregister()
"""
if __name__ == '__main__':
    rospy.init_node('color', anonymous=True)
    x_dict = {}
    y_dict = {}
    #global f
    #f = open('sphero_ip.py', 'w')
    try:
        for i in range(0,2*robot_number_inital+2):
            print(2)
            #color_callback = rospy.Subscriber('/ObjectData', Float32MultiArray, callback)
            sphero_name = 'sphero%s' % (i)
            print(sphero_name)
            x_dict[sphero_name] = []
            y_dict[sphero_name] = []
            change_color(i+1,x_dict,y_dict)
        rospy.spin()
    except KeyboardInterrupt:
        sys.exit(1)
