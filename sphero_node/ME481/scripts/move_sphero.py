#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped, Pose, Twist
from sensor_msgs.msg import Imu
import numpy as np
import math
import sys
import time
import os

import dynamic_reconfigure.server
from sphero_driver import sphero_driver2 as sphero_driver
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, TwistWithCovariance, Vector3
from sphero_node.msg import SpheroCollision
from sphero_node.msg import MotorPower
from std_msgs.msg import ColorRGBA, Float32, Bool
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from sphero_node.cfg import ReconfigConfig
from sphero_node.msg import VelGroup
import run_params

from control.matlab import *
path = os.path.split(os.getcwd())[0]
class_path = path + '/scripts/' + run_params.run_file
execfile(class_path)
 

class robot_move():

    def __init__(self):
        self.x_vel = 0
        self.y_vel = 0
        self.start_time = time.time()
        self.pub = rospy.Publisher('set_motor_commands', MotorPower, queue_size = 10)


    def move_sphero(self):
        rate = rospy.Rate(50)
        while not rospy.is_shutdown(): 
            elapsed_time = time.time() - self.start_time
            velocities = sphero_control(elapsed_time) # function located in specified file. Returns [left_power, right_power, left_state, right_state]
            left_power = velocities[0]
            right_power = velocities[1]
            left_state = velocities[2]
            right_state = velocities[3]
            motor_power = MotorPower()
            motor_power.left_power = left_power
            motor_power.left_state = left_state
            motor_power.right_power = right_power
            motor_power.right_state = right_state
            self.pub.publish(motor_power)
            rate.sleep()

        

    
    def stop(self):
        for i in range(0,self.robot_number):
            current_time = self.time_inital - time.time()
            x_vel = 0
            y_vel = 0
            self.x_vel.append(x_vel)
            self.y_vel.append(y_vel)

            vel_group.x_vel = self.x_vel
            vel_group.y_vel = self.y_vel
            self.pub.publish(vel_group)


    def normalize_angle_positive(self, angle):
        return math.fmod(math.fmod(angle, 2.0*math.pi) + 2.0*math.pi, 2.0*math.pi);
 


if __name__ == '__main__':
    rospy.init_node('move', anonymous=True)
    r = robot_move()

    try:
        r.move_sphero()
    except KeyboardInterrupt:
        stop = r.stop()
        if stop:
            sys.exit(1)
        else:
            stop = r.stop()
            sys.exit(1)

    
